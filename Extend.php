<?php

/**
 * PK框架 - 底层 扩展模块
 * User: zhanghui
 * Date: 2016/3/31
 * Time: 9:48
 */

namespace PKCore\Extend;

use PKCore\Converter;
use function PKCore\fail;
use PKCore\Files;
use PKCore\Formats;
use function PKCore\Route\language;

/**
 * 利用 SOAP 服务器创建 wsdl 文件并显示
 * @param $ctrl_path - 控制器的路径
 * @param $ctrl_file - 控制器的文件名，但不包含扩展名
 * @param $class_name - 控制器的类名
 */
function SOAPServerCreateWSDL($ctrl_path, $ctrl_file, $class_name)
{
    class_exists('SoapServer') or \PKCore\fail('php_soap extends no exists');
    $wsdl_path = PATH_TMP . 'wsdl' . DS;
    $wsdl_arr_file = 'wsdl_index.php';
    $ctrl_file .= '.php';
    $file_info = Files::baseProperty($ctrl_path . $ctrl_file);
    $ctrl_file_mTime = $file_info['m_time'];
    $ctrl_file_path_key = md5($ctrl_path . $ctrl_file);
    $wsdl_arr = array();
    $wsdl_arrFile = $wsdl_path . $wsdl_arr_file;
    if (file_exists($wsdl_arrFile)) {
        /** @noinspection PhpIncludeInspection */
        $tmp_arr = require_once "$wsdl_arrFile";
        !Formats::isArray($tmp_arr) ?: $wsdl_arr = $tmp_arr;
    }
    // 判断当前的 wsdl 数组中是否存在当前 controller 的文件路径
    $wsdl_name = str_replace('\\', '_', $class_name);
    $old_mTime = array_key_exists($ctrl_file_path_key, $wsdl_arr) ? $wsdl_arr[$ctrl_file_path_key] : 0;
    if ($old_mTime > 0 && $old_mTime == $ctrl_file_mTime) {
        $wsdl_file = $wsdl_path . $wsdl_name . '.wsdl';
    } else {
        $wsdl_arr[$ctrl_file_path_key] = $ctrl_file_mTime;
//        print_r($old_mTime); exit();
        \PKCore\Route\extend('SoapDiscovery', true);
        /**
         * 第一参数为类名，也是生成wsdl的文件名Service.wsdl
         * 第二个参数是服务的名字可以随便写
         */
        try {
            $sd = new SoapDiscovery($wsdl_path, $class_name, 'soap');
            $wsdl_file = $sd->getWSDL();
        } catch (\Exception $exception) {
            fail($exception->getMessage());
        }
        Files::putContents($wsdl_path, $wsdl_arr_file, Converter::arrayToSaveString($wsdl_arr));
    }
    if (isset($wsdl_file)) {
        $soap = new \SoapServer($wsdl_file);
        $soap->setClass($class_name);
        $soap->handle();
    }
}

/**
 * SOAP 客户端
 * @param $wsdl_url - WSDL 的 URL 路径
 * @return \SoapClient|string
 */
function soapClient($wsdl_url)
{
    class_exists('SoapClient') or \PKCore\fail('php_soap extends no exists');
    try {
        $soap = new \SoapClient($wsdl_url);
        return $soap;
    } catch (\Exception $e) {
        fail($e->getMessage());
    }
    return '';
}

/**
 * Class Zip 打包操作
 * @package PK\Extend
 */
class Zip
{

    protected $zips;

    /**
     * 制作压缩包
     * @param $content
     * @param $zipname
     */
    public function dozip($content, $zipname)
    {
        if (class_exists('ZipArchive')) {
            $this->zips = new \ZipArchive();
            if (file_exists($zipname) == FALSE) {
                $this->zips->open($zipname, \ZipArchive::OVERWRITE); //创建一个空的zip文件
                $this->for_dir($content, $content);
                $this->zips->close();
            }
        } else {
            fail('class [ZipArchive] no exists!');
        }
    }

    private function for_dir($path, $removedir)
    {
        $path = rtrim($path, '/') . '/';
        $headdir = @dir($path);
        if (!method_exists($headdir, 'read'))
            return FALSE;
        while (($file = $headdir->read()) != FALSE) {
            $adddir = substr($path . $file, strlen($removedir));
            if (strpos($file, '.') > 0 && method_exists($this->zips,'addFile')) {
                $this->zips->addFile($adddir);
            }
            if (!stristr($file, '.') && method_exists($this->zips,'addEmptyDir')) {
                $this->zips->addEmptyDir($adddir);
                $this->for_dir($path . $file, $removedir);
            }
        }
        return true;
    }

    /**
     * 读取包中某个文件的内容
     * @param $filename
     * @param $phpname
     */
    public function read($filename, $phpname)
    {
        class_exists('ZipArchive') ?: fail(language('no_zip'));
        $this->zips = new \ZipArchive();
        if ($this->zips->open($filename) == TRUE) {
            echo $this->zips->getFromName($phpname);
            $this->zips->close();
        }
    }

}


