<?php
/**
 * PK框架 - 底层 调度模块
 * User: zhanghui
 * QQ:366065186 email: zhanghui@pkcms.cn
 * Date: 2016/4/5
 * Time: 12:51
 */
include_once 'Config.php';
version_compare(phpversion(), ASK_PHP_VER, '>=') or die('php version >= ' . ASK_PHP_VER);
/**
 * 判断框架是否在站点的根目录
 * 如果是，则提醒将框架移动到根目录的二级目录下
 */
strtolower(substr(PATH_PK, strlen(PATH_ROOT))) == 'pkframe' . DS ?: exit('PK:' . PATH_PK . ' <BR/>SITE:' . PATH_ROOT . ' <BR>Please put the framework program into a new folder in the root directory of your web site');
// 自动加载文件
$autoload_file = PATH_PK . 'AutoLoad.php';
file_exists($autoload_file) ?: die('autoload file no exists');
include_once "$autoload_file";
// 初始化加载
$init_file = PATH_PK . 'Init.php';
file_exists($init_file) ?: die('init drive file no exists');
include_once "$init_file";
/**
 * 判断是否加载了配置文件，同时也是是否存在配置文件
 */
$is_loading_config = \PKCore\Route\config('Config');
if ($is_loading_config == false) {
    // 如果没有加载，或者是不存在,初始化程序
    \PKCore\Init\siteDir();
    \PKCore\Init\configInc();
    $is_loading_config = \PKCore\Route\config('Config');
} else {
    \PKCore\Init\siteDir();
}
if (defined('DEBUG_MODE')) {
    ini_set('display_errors', DEBUG_MODE ? 1 : 0);
}
@ini_set('xdebug.show_exception_trace', 0);
@ini_set('magic_quotes_runtime', 0);
// 指定允许其他域名访问
header('Access-Control-Allow-Origin:' . \PKCore\Request::origin());
// 响应头设置
if (\PKCore\Request::isMobile()) {
    header('Access-Control-Allow-Headers:X-PINGOTHER,Content-Type');
} else {
    header('Access-Control-Allow-Headers:Content-Type,authorization');
}
// 只是做验证处理s
if (\PKCore\Request::method() == 'OPTIONS') {
    \PKCore\Statics::headStatusCode(204);
    exit();
}
// 分配的内存量
if (function_exists('memory_get_usage'))
    \PKCore\Request::param('memoryUsageSize', memory_get_usage());
if (ini_get('output_buffering'))
    ini_set("output_buffering", "1");
if (defined('SOAP_WSDL_CACHE_EXPIRATION')) {
    @ini_set('soap.wsdl_cache_enabled', SOAP_WSDL_CACHE_EXPIRATION);
}
if (defined('IS_LOCAL_SESSION') && IS_LOCAL_SESSION) {
    //Session保存路径
    $sessSavePath = PATH_TMP . "sessions" . DS;
    file_exists($sessSavePath) ?: \PKCore\Files::mkdir($sessSavePath);
    if (is_writeable($sessSavePath) && is_readable($sessSavePath)) {
        session_save_path($sessSavePath);
    }
}
\PKCore\Route\gotoApps();
