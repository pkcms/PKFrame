<?php
/**
 * 数据模型驱动
 * User: wz_zh
 * Date: 2017/9/6
 * Time: 9:56
 */

namespace PKCore;

use PKCore\DbDriver\MPDO;

class DataBase
{
    private $_config = [];
    private $_sql = [];
    private $_from;
    private $_where;
    private $_order;
    private $_group;
    private $_limit;
    private $_toList = false;
    public $InsertId;
    private $_dbInitClass, $_nowDBLinkIndex;

    /**
     * DataBase constructor.
     * @param $tableName - 数据表的表名
     * @param int $linkIndex
     */
    public function __construct($tableName, $linkIndex = 0)
    {
        $this->_from = ' FROM ' . $this->_tableName($tableName, $linkIndex);
    }

    /**
     * 多链接的索引设置
     * @param int $index
     */
    private function _setDBLink($index = 0)
    {
        $this->_nowDBLinkIndex = $index;
        Request::param('NowDBLinkIndex', $index);
        if (defined('DATABASE') && is_array(DATABASE) && array_key_exists($index, DATABASE)) {
            $this->_config[$index] = DATABASE[$index];
        } else {
            fail('config param: DBLinkList is Empty Or not is array');
        }
        if (is_array($this->_dbInitClass)
            && array_key_exists($index, $this->_dbInitClass) && method_exists($this->_dbInitClass[$index], 'checkPing')) {
            // 已经存在的初始化
            $this->_dbInitClass[$index]->checkPing();
        } else {
            is_array($this->_dbInitClass) ?: $this->_dbInitClass = [];
            if (!class_exists('\pdo')) {
                isset($this->_config['type']) ?: fail('Config Param: type, is Empty');
                $driver = $this->_config['type'];
                \PKCore\Route\DbDriver($driver);
                $class_name = '\PKCore\DbDriver\\' . $driver;
                class_exists($class_name) ?: fail('DbDriver class ' . $driver . ' no exists');
                $this->_dbInitClass[$index] = new $class_name();
            } else {
                \PKCore\Route\DbDriver('MPDO');
                $this->_dbInitClass[$index] = new MPDO();
            }
        }
    }

    /**
     * 数据表的联表查询
     * @param $tableName
     * @param $joinWhere
     * @param int $linkIndex
     * @param string $joinType 连接的类型有： left join（左连接）, right join（右连接）, inner join（内连接）
     * @return $this
     */
    public function TableJoin($tableName, $joinWhere, $linkIndex = 0, $joinType = 'left')
    {
        if (stristr($this->_from, 'FROM')) {
            $this->_from .= " {$joinType} join " . $this->_tableName($tableName, $linkIndex) . " on {$joinWhere}";
        } else {
            fail('SQL FROM param is Empty');
        }
        return $this;
    }

    private function _tableName($tableName, $linkIndex = 0)
    {
        $this->_setDBLink($linkIndex);
        $config = $this->_config[$this->_nowDBLinkIndex];
        if (isset($config['type']) && isset($config['prefix']) && isset($config['name'])) {
            switch ($config['type']) {
                case 'mysql':
                    return "`{$config['name']}`.`{$config['prefix']}{$tableName}`";
                    break;
            }
        } else {
            fail('Database Config Param (type | prefix | name) not Exists');
        }
        return null;
    }

    /**
     * 组装条件SQL语句
     * @param array $options 选项数据
     * @param string $logic 关系词 And | Or
     * @return $this
     */
    public function Where($options = array(), $logic = 'and')
    {
        $this->_where = ' WHERE ';
        if (Formats::isArray($options)) {
            $temp = array();
            $config = $this->_config[$this->_nowDBLinkIndex];
            if (isset($config['type'])) {
                switch ($config['type']) {
                    case 'mysql':
                        break;
                }
            }
            foreach ($options as $key => $value) {
                if (Formats::isArray($value)) {
                    $v_arr = array();
                    foreach ($value as $v) {
                        // 判断是否为数字,非数字则要带上引号
                        $v_arr[] = Formats::isNumeric($v) ? $v : '"' . $v . '"';
                    }
                    $value = Formats::isArray($v_arr) ? implode(',', $v_arr) : '';
                    $temp[] = $key . ' IN (' . $value . ')';
                } else {
                    if (is_string($key)) {
                        switch (\gettype($value)) {
                            case 'boolean':
                                $temp[] = "{$key}=" . ($value ? "TRUE" : "FALSE");
                                break;
                            default:
                                if (!is_array($value)) {
                                    $temp[] = $key . '=' . (!empty($value) && stristr('"', $value) ? $value : '"' . $value . '"');
                                }
                                break;
                        }
                    } else {
                        $temp[] = $value;
                    }
                }
            }
            $this->_where .= implode(' ' . strtoupper($logic) . ' ', $temp);
        }
        return $this;
    }

    /***
     * 设置查询数据时的分组字段
     * @param string $groupBy 按某个指定字段的数据进行分组
     * @return $this
     */
    public function GroupBy($groupBy)
    {
        $this->_group = " GROUP BY `{$groupBy}`";
        return $this;
    }

    /**
     * 排序
     * @param $orderByField
     * @param string $orderByMode (ASC | DESC)
     * @return $this
     */
    public function OrderBy($orderByField, $orderByMode = 'DESC')
    {
        empty($this->_order) ? $this->_order = " ORDER BY {$orderByField} {$orderByMode}": $this->_order .= ",{$orderByField} {$orderByMode}";
        return $this;
    }

    /**
     * 分页
     * @param $rowSize
     * @param $index
     * @return $this
     */
    public function Limit($rowSize, $index = 0)
    {
        $this->_limit = " LIMIT " . ($index == 0 ? $rowSize : ($index - 1) * $rowSize . ',' . $rowSize);
        return $this;
    }

    /**
     * 在数据表中查询数据的所有记录
     * @param mixed ...$param
     * @return $this
     */
    public function Select(...$param)
    {
        if (Formats::isArray($param)) {
            $select = implode(',', $param);
        } elseif (\count($param) == 0) {
            $select = '*';
        } else {
            $select = $param;
        }
        $sql = "SELECT {$select}{$this->_from}";
        empty($this->_where) ?: $sql .= $this->_where;
        empty($this->_order) ?: $sql .= $this->_order;
        empty($this->_group) ?: $sql .= $this->_group;
        empty($this->_limit) ?: $sql .= $this->_limit;
        $this->_sql[] = $sql;
        return $this;
    }

    public function First()
    {
        $this->_toList = false;
        if (!Formats::isArray($this->_sql)) {
            fail('Do Query SQL is Empty');
        }
        $sql_str = strtolower($this->_sql[0]);
        $class = $this->_dbInitClass[$this->_nowDBLinkIndex];
        if (strstr($sql_str, 'select') && method_exists($class, 'fetchAssoc')) {
            return $class->fetchAssoc($this->_sql[0], $this->_toList);
        }
        return $this;
    }

    public function ToList()
    {
        $this->_toList = true;
        if (!Formats::isArray($this->_sql)) {
            fail('Do Query SQL is Empty');
        }
        $sql_str = strtolower($this->_sql[0]);
        $class = $this->_dbInitClass[$this->_nowDBLinkIndex];
        if (strstr($sql_str, 'select') && method_exists($class, 'fetchAssoc')) {
            return $class->fetchAssoc($this->_sql[0], $this->_toList);
        }
        return $this;
    }

    /**
     * 数据表数据量的统计
     * @param string $fieldAS
     * @param string $field
     * @return $this
     */
    public function Count($fieldAS = 'count', $field = '*')
    {
        $sql = "SELECT COUNT({$field}) AS {$fieldAS}{$this->_from}";
        empty($this->_where) ?: $sql .= $this->_where;
        empty($this->_group) ?: $sql .= $this->_group;
        $this->_sql[] = $sql;
        return $this;
    }

    // ============================ 数据设置

    public function Sql($sql)
    {
        $this->_sql[] = empty($this->_limit) ? $sql : $sql . $this->_limit;
        return $this;
    }

    /**
     * 将提交的数组转换为 insert SQL语句
     * @param array $data 被 INSERT 数组
     * @return $this
     */
    public function Insert($data = [])
    {
        $field = $value = '';
        if (is_object($data)) {
            $data = Converter::objectToArray($data);
        }
        if (Formats::isArray($data) && isset($data[0])) {
            $valueArr = array();
            foreach ($data as $item) {
                list($field, $valueArr[]) = self::_insertOrReplaceDataJoin($item);
            }
            $value = Formats::isArray($valueArr) ? implode(',', $valueArr) : '';
        } elseif (Formats::isArray($data)) {
            list($field, $value) = self::_insertOrReplaceDataJoin($data);
        }
        if (!empty($field) && !empty($value)) {
            $table = trim(str_replace('FROM', '', $this->_from));
            $this->_sql[] = "INSERT " . "INTO {$table} ({$field}) VALUES {$value}";
        }
        return $this;
    }

    /**
     * 将提交的数组转换成 UPDATE SQL 语句
     * @param array $data 被 UPDATE 的数组
     * @return $this
     */
    public function Update($data = [])
    {
        $setArr = array();
        if (is_object($data)) {
            $data = Converter::objectToArray($data);
        }
        $config = $this->_config[$this->_nowDBLinkIndex];
        foreach ($data as $key => $value) {
            if (isset($config['type'])) {
                switch ($config['type']) {
                    case 'mysql':
                        switch (\gettype($value)) {
                            case 'boolean':
                                $item = "`{$key}`=" . ($value ? "TRUE" : "FALSE");
                                break;
                            default:
                                $item = "`{$key}`=" . "'" . str_replace("'", "\'", $value) . "'";
                                break;
                        }
                        break;
                }
            }
            !isset($item) ?: $setArr[] = $item;
        }
        if (Formats::isArray($setArr)) {
            $set = implode(',', $setArr);
            $table = trim(str_replace('FROM', '', $this->_from));
            $sql = "UPDATE" . " {$table} SET {$set}";
            empty($this->_where) ?: $sql .= $this->_where;
            $this->_sql[] = $sql;
        }
        return $this;
    }

    /**
     * REPLACE 批量数据更新
     * @param array $data
     * @return $this
     */
    public function Replace($data = [])
    {
        $field = $value = '';
        if (is_object($data)) {
            $data = Converter::objectToArray($data);
        }
        if (Formats::isArray($data) && isset($data[0])) {
            $valueArr = array();
            foreach ($data as $item) {
                list($field, $valueArr[]) = self::_insertOrReplaceDataJoin($item);
            }
            $value = implode(',', $valueArr);
        } elseif (Formats::isArray($data)) {
            list($field, $value) = self::_insertOrReplaceDataJoin($data);
        }
        if (!empty($field) && !empty($value)) {
            $table = trim(str_replace('FROM', '', $this->_from));
            $this->_sql[] = "REPLACE" . " INTO {$table} ({$field}) VALUES {$value}";
        }
        return $this;
    }

    /**
     * 组合提交的数组为 INSERT SQL 组合
     * @param array $data 被 INSERT 数组
     * @return array
     */
    private function _insertOrReplaceDataJoin($data = [])
    {
        $fieldArr = $dataArr = array();
        $field = $value = '';
        $config = $this->_config[$this->_nowDBLinkIndex];
        if (Formats::isArray($data)) {
            foreach ($data as $key => $value) {
                if (empty($key)) {
                    continue;
                }
                $fieldArr[] = $key;
                $dataArr[] = str_replace("'", "\'", $value);
            }
            if (isset($config['type'])) {
                switch ($config['type']) {
                    case 'mysql':
                        $field = '`' . implode('`,`', $fieldArr) . '`';
                        $value = "('" . implode("','", $dataArr) . "')";
                        break;
                }
            }
        }
        return array($field, $value);
    }

    /**
     * 删除数据表中的指定数据
     * @return $this
     */
    public function Delete()
    {
        $sql = "DELETE {$this->_from}";
        empty($this->_where) ?: $sql .= $this->_where;
        $this->_sql[] = $sql;
        return $this;
    }

    /**
     * 清空数据表中的指定数据
     * @return $this
     */
    public function Truncate()
    {
        $this->_from = str_replace(' FROM ', '', $this->_from);
        $sql = "TRUNCATE {$this->_from}";
        $this->_sql[] = $sql;
        return $this;
    }

    /**
     * 执行 SQL 语句
     */
    public function Query()
    {
        if (!Formats::isArray($this->_sql)) {
            fail('Do Query SQL is Empty');
        }
        $sql_str = strtolower($this->_sql[0]);
        $class = $this->_dbInitClass[$this->_nowDBLinkIndex];
        if (strstr($sql_str, 'insert') && method_exists($class, 'insertId')) {
            return $class->insertId($this->_sql[0]);
//        } elseif (strstr($sql_str, 'select') && method_exists($class, 'fetchAssoc')) {
//            return $class->fetchAssoc($this->_sql[0], $this->_toList);
        } elseif (method_exists($class, 'sqlQuery')) {
            return $class->sqlQuery($this->_sql[0]);
        }
        return null;
    }

    public function BatchRun()
    {
        if (!Formats::isArray($this->_sql)) {
            fail('Do Query SQL is Empty');
        }
        $sql_str = implode(';', $this->_sql);
        $class = $this->_dbInitClass[$this->_nowDBLinkIndex];
        if (method_exists($class, 'sqlQuery')) {
            $class->sqlQuery($sql_str);
        }
    }

}

function DB($tableName, $linkIndex = 0)
{
    return new DataBase($tableName, $linkIndex);
}