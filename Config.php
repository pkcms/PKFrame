<?php
/**
 * 底层配置
 * User: wz_zh
 * Date: 2017/8/11
 * Time: 9:32
 */

error_reporting(E_ALL & ~E_NOTICE);
header("Access-Control-Allow-Credentials:true");
header("Connection: keep-alive");
header('Access-Control-Max-Age: 86400');
// 响应类型
header('Access-Control-Allow-Methods:get,post,put');
date_default_timezone_set('PRC');
define('SYS_TIME', microtime(TRUE));
define('ASK_PHP_VER', '7.0.0');
define('DS', DIRECTORY_SEPARATOR);
$httpType = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https' : 'http';
define('DOMAIN', $httpType . '://' . $_SERVER['HTTP_HOST'] . (in_array($_SERVER['SERVER_PORT'], array('80','443')) ? '' : ':' . $_SERVER['SERVER_PORT']) . '/');

define('TIMESTAMP', time());
define('PATH_PK', dirname(__FILE__) . DS);
define('PATH_ROOT', dirname(PATH_PK) . DS);
define('PATH_TMP', PATH_ROOT . 'Tmp' . DS);
