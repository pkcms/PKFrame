<?php
/**
 * 静态处理
 * User: wz_zh
 * Date: 2017/7/13
 * Time: 9:46
 */

namespace PKCore;

use PKCore\Route;

Route\extend('Curl1', true);
Route\extend('Fsocketopen', true);

use PKCore\Extend\Fsocketopen;
use function PKCore\Route\language;

class Statics
{

    /**
     * 将 HTML 转换成 ASCII 码
     * @param string $html_code HTML 代码
     * @return mixed|string
     */
    public static function toASCII($html_code = '')
    {
        if ($html_code != "") {
            $html_code = str_replace(">", ">", $html_code);
            $html_code = str_replace("<", "<", $html_code);
            $html_code = str_replace(" ", chr(32), $html_code);
            $html_code = str_replace("", chr(13), $html_code);
            $html_code = str_replace("<br>", chr(10) & chr(10), $html_code);
            $html_code = str_replace("<BR>", chr(10), $html_code);
        }
        return $html_code;
    }

    /**
     * 从文章正文中提取图片路径
     * @param string $html_code
     * @return bool|mixed
     */
    public static function imgsinarticle($html_code = '')
    {
        $temp = array();
        preg_match_all("/(src|SRC)=[\"|'| ]{0,}((.*).(gif|jpg|jpeg|bmp|png))/isU", $html_code, $temp);
        if (is_array($temp[0]) == FALSE) {
            return FALSE;
        }
        foreach ($temp[0] as $key => $value) {
            $temp[0][$key] = substr($value, 5);
        }
        return $temp[0];
    }

    /**
     * 对 HTML 代码进行压缩
     * @param string $data 要处理的内容信息
     * @return string
     */
    public static function zip($data)
    {
        if (strlen($data) > 2) {
            $data = preg_replace("/[\r|\n|\r\n]/", "", $data);
            $data = preg_replace("/\/\/[\S\f\t\v ]*?;[\r|\n]/", "", $data); // js注释请以";"号结尾
            // $data=preg_replace("/\<\!\-\-[\s\S]*?\-\-\>/","",$data);//去掉html里的<!--注释-->
            $data = preg_replace("/\>[\s]+\</", "><", $data);
            $data = preg_replace("/;[\s]+/", ";", $data);
            $data = preg_replace("/[\s]+\}/", "}", $data);
            $data = preg_replace("/}[\s]+/", "}", $data);
            $data = preg_replace("/\{[\s]+/", "{", $data);
            $data = preg_replace("/([\s]){2,}/", "$1", $data);
            $data = preg_replace("/[\s]+\=[\s]+/", "=", $data);
            $data = str_replace('	', '', $data);
            return $data;
        } else {
            return "";
        }
    }

    /**
     * 分页的处理
     * @param int $data_count 总数据量
     * @param int $line_number 返回列的行数
     * @param int $page_code_index 当前的页码
     * @param string $link 翻页
     * @return array
     */
    public static function pages($data_count = 0, $line_number = 0, $page_code_index = 0, $link = '')
    {
        $page_code_index = max($page_code_index, 1);
        // 页码总数
        $total_page_size = 5;
        //最后页，也是总页数
        $page_count = ceil($data_count / $line_number);
        //上一页
        $pre_page_code = max($page_code_index - 1, 1);
        // 下一页
        $next_page_code = $page_code_index + 1;
        // 判断下一页是否为最后一页
        $next_page_code < $page_count ?: $next_page_code = $page_count;
        // 产生页码区间
        // 产生最小和最大的值
        $min = max($page_code_index - 2, 1);
        $max = $page_code_index + 2;
        // 判断设置显示页数 大于 页面总数
        if ($total_page_size > $page_count)
            $max = min($max, $page_count);
        // 当设置显示数 小于 页面总数时，同时判断下一页的页数是否超过了总数
        if ($total_page_size < $page_count) {
            $min = min($page_count - $total_page_size + 1, $min);
            $max = $max > $page_count ? $page_count : max($max, $total_page_size);
        }
        $page_section = range($min, $max);
        // 产生 HTML
        if ($page_count == 0) return [];
        $result = [
            ['start', str_replace('[page]', 1, $link)],
            ['pre', str_replace('[page]', $pre_page_code, $link)],
        ];
        foreach ($page_section as $number) {
            $result[] = [$number, str_replace('[page]', $number, $link)];
        }
        $result[] = ['next', str_replace('[page]', $next_page_code, $link)];
        $result[] = ['end', str_replace('[page]', $page_count, $link)];
        return $result;
    }

    /**
     * 对http协议的状态设定，跳转页面中需要经常使用的函数
     * @param string $code 状态码
     */
    public static function headStatusCode($code = '')
    {
        static $_status = array(
            // Informational 1xx
            100 => 'Continue',
            101 => 'Switching Protocols',
            // Success 2xx
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            // Redirection 3xx
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Moved Temporarily ', // 1.1
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            // 306 is deprecated but reserved
            307 => 'Temporary Redirect',
            // Client Error 4xx
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            // Server Error 5xx
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
            509 => 'Bandwidth Limit Exceeded'
        );
        if (array_key_exists($code, $_status)) {
            header('HTTP/1.1 ' . $code . ' ' . $_status[$code]);
        }
    }

    public static function resultJsonModel($data = '', $success = true, $code = null)
    {
        header("Content-type:application/json; charset=UTF-8");
        if (defined('RESULT_JSON_MODEL') && array_key_exists('resultField', RESULT_JSON_MODEL)
            && array_key_exists('stateField', RESULT_JSON_MODEL)
            && array_key_exists('msgField', RESULT_JSON_MODEL)
            && array_key_exists('codeField', RESULT_JSON_MODEL)) {
            $m = [
                RESULT_JSON_MODEL['resultField'] => $success ? $data : '',
                RESULT_JSON_MODEL['stateField'] => $success,
                RESULT_JSON_MODEL['msgField'] => $success ? '' : $data,
                RESULT_JSON_MODEL['codeField'] => $success ? 200 : ($code == null ? 500 : $code),
            ];
        } else {
            $m = [
                'Result' => $success ? $data : '',
                'State' => $success,
                'Msg' => $success ? '' : $data,
                'Code' => $success ? 200 : ($code == null ? 500 : $code),
            ];
        }
        $m['Request'] = $_SERVER['REQUEST_URI'];
        die(Converter::jsonEnCode($m));
    }

    public static function resultWebPageModel($data = '', $statusCode = 200)
    {
        header("Content-type:text/html;charset=utf-8");
        self::headStatusCode($statusCode);
        die($data);
    }

    public static function error($code = 500000, $tips = null, $errCode = 500)
    {
        $str = language($code);
        if (!is_null($tips)) {
            $str .= ' ' . $tips;
        }
        if (Request::isAjax()) {
            self::resultJsonModel($str, false, $errCode);
        } else {
            self::resultWebPageModel($code, $errCode);
        }
    }

    /**
     * 关闭窗口脚本
     */
    public static function closeWin()
    {
        self::resultWebPageModel('<script>window.opener=null;window.open(\'\',\'_self\');window.close();</script>');
    }

    /**
     * 获取 WEB 页的代码
     * @param string $url URL 地址
     * @return bool|string
     */
    public static function getWebCode($url = '')
    {
        if (empty($url)) {
            return false;
        }
        $data = '';
        for ($i = 0; $i < 5 && empty($data); $i++) {
            if (function_exists('fsockopen')) {
                $data = Fsocketopen::getHttpCode($url);
            } elseif (function_exists('file_get_contents')) {
                $data = Files::getContents($url);
            } else {
                return false;
            }
        }
        return $data;
    }

}