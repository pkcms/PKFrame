<?php
/**
 * 请求处理类
 * User: wz_zh
 * Date: 2017/7/5
 * Time: 16:23
 */

namespace PKCore;

class Request
{
    /**
     * @var string 域名（含协议和端口）
     */
    private static $domain;

    /**
     * @var string URL地址
     */
    private static $url;

    /**
     * @var string 基础URL
     */
    private static $baseUrl;

    /**
     * @var string 当前执行的文件
     */
    private static $baseFile;

    /**
     * @var string 访问的ROOT地址
     */
    private static $root;

    /**
     * @var string pathInfo
     */
    private static $pathInfo;

    /**
     * @var string pathInfo（不含后缀）
     */
//    private static $path;

    /**
     * @var array 当前路由信息
     */
    private static $routeInfo = [];

    /**
     * @var array 当前调度信息
     */
    private static $dispatch = [];
    private static $module;
    private static $controller;
    private static $action;
    // 当前语言集
    private static $langSet;

    /**
     * @var array 请求参数
     */
    private static $param = [];
    private static $get = [];
    private static $post = [];
    private static $input = '';
    private static $request = [];
    private static $route = [];
    private static $put;
    private static $file = [];
    private static $server = [];
    private static $header = [];
    private static $env = [];

    /**
     * @var array 资源类型
     */
    private static $mimeType = [
        'xml' => 'application/xml,text/xml,application/x-xml',
        'json' => 'application/json,text/x-json,application/jsonrequest,text/json',
        'js' => 'text/javascript,application/javascript,application/x-javascript',
        'css' => 'text/css',
        'rss' => 'application/rss+xml',
        'yaml' => 'application/x-yaml,text/yaml',
        'atom' => 'application/atom+xml',
        'pdf' => 'application/pdf',
        'text' => 'text/plain',
        'image' => 'image/png,image/jpg,image/jpeg,image/pjpeg,image/gif,image/webp,image/*',
        'csv' => 'text/csv',
        'html' => 'text/html,application/xhtml+xml,*/*',
    ];

    /**
     * 设置或获取当前包含协议的域名
     * @access public
     * @param string $domain 域名
     * @return string
     */
    public static function domain($domain = null)
    {
        if (!is_null($domain)) {
            self::$domain = $domain;
        } elseif (empty(self::$domain)) {
            self::$domain = self::scheme() . '://' . self::host();
        }
        return self::$domain;
    }

    /**
     * 获取 origin
     * @return string
     */
    public static function origin()
    {
        return self::header('origin');
    }

    /**
     * 设置或获取当前完整URL 包括QUERY_STRING
     * @access public
     * @param string|true $url URL地址 true 带域名获取
     * @return string
     */
    public static function url($url = null)
    {
        if (!is_null($url) && true !== $url) {
            self::$url = $url;
        } elseif (is_null(self::$url)) {
            if (isset($_SERVER['HTTP_X_REWRITE_URL'])) {
                self::$url = $_SERVER['HTTP_X_REWRITE_URL'];
            } elseif (isset($_SERVER['REQUEST_URI'])) {
                self::$url = $_SERVER['REQUEST_URI'];
            } elseif (isset($_SERVER['ORIG_PATH_INFO'])) {
                self::$url = $_SERVER['ORIG_PATH_INFO'] . (!empty($_SERVER['QUERY_STRING']) ? '?' . $_SERVER['QUERY_STRING'] : '');
            } else {
                self::$url = '';
            }
        }
        return true === $url ? self::domain() . self::$url : ltrim(self::$url, '/index.php');
    }

    /**
     * 设置或获取当前URL 不含QUERY_STRING
     * @access public
     * @param string $url URL地址
     * @return string
     */
    public static function baseUrl($url = null)
    {
        if (!is_null($url) && true !== $url) {
            self::$baseUrl = $url;
        } elseif (!self::$baseUrl) {
            $str = self::url();
            self::$baseUrl = strpos($str, '?') ? strstr($str, '?', true) : $str;
        }
        return true === $url ? self::domain() . self::$baseUrl : self::$baseUrl;
    }

    /**
     * 设置或获取当前执行的文件 SCRIPT_NAME
     * @access public
     * @param string $file 当前执行的文件
     * @return string
     */
    public static function baseFile($file = null)
    {
        if (!is_null($file) && true !== $file) {
            self::$baseFile = $file;
        } elseif (!self::$baseFile) {
            $url = '';
            $script_name = basename($_SERVER['SCRIPT_FILENAME']);
            if (basename($_SERVER['SCRIPT_NAME']) === $script_name) {
                $url = $_SERVER['SCRIPT_NAME'];
            } elseif (basename($_SERVER['PHP_SELF']) === $script_name) {
                $url = $_SERVER['PHP_SELF'];
            } elseif (isset($_SERVER['ORIG_SCRIPT_NAME']) && basename($_SERVER['ORIG_SCRIPT_NAME']) === $script_name) {
                $url = $_SERVER['ORIG_SCRIPT_NAME'];
            } elseif (($pos = strpos($_SERVER['PHP_SELF'], '/' . $script_name)) !== false) {
                $url = substr($_SERVER['SCRIPT_NAME'], 0, $pos) . '/' . $script_name;
            } elseif (isset($_SERVER['DOCUMENT_ROOT']) && strpos($_SERVER['SCRIPT_FILENAME'], $_SERVER['DOCUMENT_ROOT']) === 0) {
                $url = str_replace('\\', '/', str_replace($_SERVER['DOCUMENT_ROOT'], '', $_SERVER['SCRIPT_FILENAME']));
            }
            self::$baseFile = $url;
        }
        return true === $file ? self::domain() . self::$baseFile : self::$baseFile;
    }

    /**
     * 设置或获取URL访问根地址
     * @access public
     * @param string $url URL地址
     * @return string
     */
    public static function root($url = null)
    {
        if (!is_null($url) && true !== $url) {
            self::$root = $url;
        } elseif (!self::$root) {
            $file = self::baseFile();
            if ($file && 0 !== strpos(self::url(), $file)) {
                $file = str_replace('\\', '/', dirname($file));
            }
            self::$root = rtrim($file, '/');
        }
        return true === $url ? self::domain() . self::$root : self::$root;
    }

    /**
     * 获取当前请求URL的pathinfo信息（含URL后缀）
     * @access public
     * @return string
     */
    public static function pathInfo()
    {
        if (is_null(self::$pathInfo)) {
            // 分析PATHINFO信息
            if (isset($_SERVER['ORIG_PATH_INFO'])) {
                self::$pathInfo = ltrim($_SERVER['ORIG_PATH_INFO'], '/');
            } elseif (isset($_SERVER['PATH_INFO'])) {
                self::$pathInfo = ltrim($_SERVER['PATH_INFO'], '/');
            } else {
                self::$pathInfo = '/';
            }
        }
        return self::$pathInfo;
    }

    /**
     * 当前URL的访问后缀
     * @access public
     * @return string
     */
    public static function ext()
    {
        return pathinfo(self::pathinfo(), PATHINFO_EXTENSION);
    }

    /**
     * 获取当前请求的时间
     * @access public
     * @param bool $float 是否使用浮点类型
     * @return integer|float
     */
    public static function time($float = false)
    {
        return $float ? self::server('REQUEST_TIME_FLOAT') : self::server('REQUEST_TIME');
    }

    /**
     * 当前请求的资源类型
     * @access public
     * @return null|string
     */
    public static function type()
    {
        $accept = self::server('HTTP_ACCEPT');
        if (is_null($accept)) {
            return null;
        }
        foreach (self::$mimeType as $key => $val) {
            $array = explode(',', $val);
            if (array_search($accept, $array)) {
                return $key;
            }
        }
        return null;
    }

    /**
     * 设置资源类型
     * @access public
     * @param string|array $type 资源类型名
     * @param string $val 资源类型
     * @return void
     */
    public static function mimeType($type, $val = '')
    {
        if (is_array($type)) {
            self::$mimeType = array_merge(self::$mimeType, $type);
        } else {
            self::$mimeType[$type] = $val;
        }
    }

    /**
     * 是否存在某个请求参数
     * @access public
     * @param string $name 变量名
     * @param string $type 变量类型
     * @param bool $checkEmpty 是否检测空值
     * @return mixed
     */
    public static function has($name, $type = 'param', $checkEmpty = true)
    {
        if (version_compare(phpversion(), '7.0.0', '>=')) {
            $param = isset(self::${$type}) && !empty(self::${$type}) ? self::${$type} : [];
        } else {
            $param = isset(self::$$type) && !empty(self::$$type) ? self::$$type : [];
        }
        $result = false;
        // 按.拆分成多维数组进行判断
        if (stristr($name, ',')) {
            $nameArr = explode(',', $name);
            foreach ($nameArr as $val) {
                $isSet = isset($param[$val]);
                $result = $checkEmpty ? ($isSet && !is_null($param[$val]) && $param[$val] == '') : ($isSet || !empty($checkEmpty));
                if (!$result) {
                    break;
                }
            }
        } else {
            $result = $checkEmpty ? isset($param[$name]) && !is_null($param[$name]) && $param[$name] != '' : isset($param[$name]);
        }
        return $result;
    }

    /**
     * 当前的请求类型
     * @access public
     * @return string
     */
    public static function method()
    {
        return self::server('REQUEST_METHOD');
    }

    /**
     * 是否为GET请求
     * @access public
     * @return bool
     */
    public static function isGet()
    {
        return self::method() == 'GET';
    }

    /**
     * 是否为POST请求
     * @access public
     * @return bool
     */
    public static function isPost()
    {
        return self::method() == 'POST';
    }

    /**
     * 是否为PUT请求
     * @access public
     * @return bool
     */
    public static function isPut()
    {
        return self::method() == 'PUT';
    }

    /**
     * 是否为DELTE请求
     * @access public
     * @return bool
     */
    public static function isDelete()
    {
        return self::method() == 'DELETE';
    }

    /**
     * 是否为HEAD请求
     * @access public
     * @return bool
     */
    public static function isHead()
    {
        return self::method() == 'HEAD';
    }

    /**
     * 是否为PATCH请求
     * @access public
     * @return bool
     */
    public static function isPatch()
    {
        return self::method() == 'PATCH';
    }

    /**
     * 是否为OPTIONS请求
     * @access public
     * @return bool
     */
    public static function isOptions()
    {
        return self::method() == 'OPTIONS';
    }

    /**
     * 是否为cli
     * @access public
     * @return bool
     */
    public static function isCli()
    {
        return PHP_SAPI == 'cli';
    }

    /**
     * 是否为cgi
     * @access public
     * @return bool
     */
    public static function isCgi()
    {
        return strpos(PHP_SAPI, 'cgi') === 0;
    }

    /**
     * 获取当前请求的参数
     * @access public
     * @param string|array $name 变量名
     * @param mixed $default 默认值
     * @return mixed
     */
    public static function param($name = '', $default = null)
    {
        if (empty(self::$param)) {
            $method = self::method();
            // 自动获取请求变量
            switch ($method) {
                case 'POST':
                    $vars = self::post();
                    break;
                case 'PUT':
                case 'DELETE':
                case 'PATCH':
                    $vars = self::put();
                    break;
                default:
                    $vars = [];
            }
            // 当前请求参数和URL地址中的参数合并
            if (Formats::isArray($vars) && Formats::isArray(self::get())) {
                self::$param = array_merge(self::get(), $vars);
            }
        }
        // 获取包含文件上传信息的数组
        self::file($name);
        self::$param = Formats::isArray(self::$file) ? array_merge(self::$param, self::$file) : self::$param;
        if (!empty($name) && !is_null($default)) {
            self::$param[$name] = Fileter::filterValue($default);
        } elseif (!empty($name) && is_array($name)) {
            self::$param = array_merge(self::$param, $name);
        } elseif (!empty($name) && is_null($default)) {
            return array_key_exists($name, self::$param) ? self::$param[$name] : null;
        }
        return self::$param;
    }

    /**
     * 设置获取路由参数
     * @access public
     * @param string|array $name 变量名
     * @param mixed $default 默认值
     * @return mixed
     */
    public static function route($name = '', $default = null)
    {
        if (empty(self::$route)) {
            $parts = preg_split('#\?#i', self::url(), 2);
            !stristr($parts[0], '.') ?: die('route url format error');
            self::routeInfo(trim($parts[0], "/"));
            self::request(empty(self::$routeInfo) ? array() : self::$routeInfo);
            if (defined('ROUTE_DEFAULT_MODULE')) {
                self::module(ROUTE_DEFAULT_MODULE);
            } else {
                fail('config param: ROUTE_DEFAULT_MODULE is Empty');
            }
            if (defined('ROUTE_DEFAULT_CONTROLLER')) {
                self::controller(ROUTE_DEFAULT_CONTROLLER);
            } else {
                fail('config param: ROUTE_DEFAULT_CONTROLLER is Empty');
            }
            self::action('main');
            if (count(self::$request) > 0) {
                !isset(self::$request[0]) ?: self::module(self::$request[0]);
                !isset(self::$request[1]) ?: self::controller(self::$request[1]);
                if (isset(self::$request[2])) {
                    self::action(self::$request[2]);
                }
            }
        }
        if (is_array($name)) {
            return self::$route = array_merge(self::$route, $name);
        }
        if (!empty($name) && !is_null($default)) {
            self::$route[$name] = Fileter::filterValue($default);
        } elseif (!empty($name) && is_null($default)) {
            return self::$route[$name];
        }
        return self::$route;
    }

    /**
     * 循环检查
     * @param $array
     * @param bool $blank 是否去空格
     * @param bool $ishtml
     * @return array
     */
    public static function fileter($array = array(), $blank = FALSE, $ishtml = FALSE)
    {
        $result = array();
        if (is_array($array) == FALSE) {
            return $result;
        }
        foreach ($array as $key => $value) {
            if (Formats::isArray($value)) {
                $result [Fileter::filterValue($key)] = self::fileter($value);
            } else {
                $value = $blank ? trim($value) : $value;
                $result [Fileter::filterValue($key, $ishtml)] = Fileter::filterValue($value, $ishtml);
            }
        }
        return $result;
    }

    /**
     * 设置获取GET参数
     * @access public
     * @param string|array $name 变量名
     * @param mixed $default 默认值
     * @return mixed
     */
    public static function get($name = '', $default = null)
    {
        if (empty(self::$get)) {
            self::$get = $_GET;
            if (!Formats::isArray(self::$get)) {
                $arr = explode('?', $_SERVER['REQUEST_URI']);
                !isset($arr[1]) ?: self::$get = Converter::stringToArray($arr[1]);
            }
        }
        if (is_array($name)) {
            return self::$get = array_merge(self::$get, $name);
        }
        if (!empty($name) && !is_null($default)) {
            self::$get[$name] = Fileter::filterValue($default);
        } elseif (!empty($name) && is_null($default)) {
            return isset(self::$get[$name]) ? self::$get[$name] : null;
        }
        return self::$get;
    }

    /**
     * 设置获取POST参数
     * @access public
     * @param string $name 变量名
     * @param mixed $default 默认值
     * @return mixed
     */
    public static function post($name = '', $default = null)
    {
        if (empty(self::$post)) {
            self::input();
            if (empty($_POST) && (false !== strpos(self::contentType(), 'application/json')
                    || false !== strpos(self::contentType(), 'text/plain'))) {
                try {
                    self::$post = json_decode(self::$input, FILTER_VALIDATE_BOOLEAN);
                } catch (\Exception $exception) {
                    fail($exception->getMessage());
                }
            } else {
                self::$post = $_POST;
            }
        }
        if (is_array($name)) {
            return self::$post = array_merge(self::$post, $name);
        }
        if (!empty($name) && !is_null($default)) {
            self::$post[$name] = Fileter::filterValue($default);
        } elseif (!empty($name) && isset(self::$post[$name]) && is_null($default)) {
            return self::$post[$name];
        } elseif (!empty($name) && is_array(self::$post) && !array_key_exists($name, self::$post)) {
            return '';
        }
        return self::$post;
    }

    /**
     * 保存 php://input
     */
    public static function input()
    {
        self::$input = file_get_contents('php://input');
        return self::$input;
    }

    /**
     * 设置获取PUT参数
     * @access public
     * @param string|array $name 变量名
     * @param mixed $default 默认值
     * @return mixed
     */
    public static function put($name = '', $default = null)
    {
        self::input();
        if (is_null(self::$put)) {
            if (empty($_POST) && (false !== strpos(self::contentType(), 'application/json')
                    || false !== strpos(self::contentType(), 'text/plain'))) {
                self::$put = json_decode(self::$input, true);
            } else {
                parse_str(self::$input, self::$put);
            }
        }
        if (is_array($name)) {
            self::$put = is_null(self::$put) ? $name : array_merge(self::$put, $name);
        } elseif (is_string($name) && !is_null($default)) {
            self::$put[$name] = Fileter::filterValue($default);
        }
        return self::$put;
    }

    /**
     * 设置获取DELETE参数
     * @access public
     * @param string|array $name 变量名
     * @param mixed $default 默认值
     * @return mixed
     */
    public static function delete($name = '', $default = null)
    {
        return self::put($name, $default);
    }

    /**
     * 设置获取PATCH参数
     * @access public
     * @param string|array $name 变量名
     * @param mixed $default 默认值
     * @return mixed
     */
    public static function patch($name = '', $default = null)
    {
        return self::put($name, $default);
    }

    /**
     * 获取request变量
     * @param string $name 数据名称
     * @param string $default 默认值
     * @return mixed
     */
    public static function request($name = '', $default = null)
    {
        if (empty(self::$request)) {
            self::$request = $_REQUEST;
        }
        if (is_array($name)) {
            return self::$request = array_merge(self::$request, $name);
        } elseif (is_string($name) && !is_null($default)) {
            self::$request[$name] = $default;
        }
        return isset(self::$request[$name]) ? self::$request[$name] : self::$request;
    }

    /**
     * 获取或设置 session 数据
     * @param string $name 数据名称
     * @param string $default 默认值
     * @param bool $is_del 是否删除
     * @return null
     */
    public static function session($name = '', $default = null, $is_del = false)
    {
        $session = null;
        ob_start();
        isset($_SESSION) ?: session_start();
        if (is_null($default) && $is_del == true && isset($_SESSION[$name])) {
            unset($_SESSION[$name]);
        } elseif (is_null($default) && $is_del == false && isset($_SESSION[$name])) {
            $session = $_SESSION[$name];
        } elseif (!is_null($default) && $is_del == false) {
            $_SESSION[$name] = $session = $default;
        }
        ob_end_clean();
        return $session;
    }

    /**
     * 获取cookie参数
     * @param string|array $name 变量名
     * @param null $default 数据
     * @param int $time 有效期
     * @param bool $is_auth_code 是否安全码处理
     * @return null
     */
    public static function cookie($name = '', $default = null, $time = 0, $is_auth_code = true)
    {
        if (empty($default) && isset($_COOKIE[$name])) {
            // 清空指定的 cookies key
            return setcookie($name, '', SYS_TIME - 3600, '/', self::domain());
        }
        $key = PATH_PK . 'cookies' . $name;
        if (is_null($default) && isset($_COOKIE[$name])) {
            // 获取指定的 cookies key
            return !$is_auth_code ? $_COOKIE[$name]
                : Fileter::base64Encode($default, false, $key);
        }
        if (!is_null($default)) {
            // 设置指定 cookies key 的 value
            !$is_auth_code ?: $default = Fileter::base64Encode($default, true, $key);
            setcookie($name, $default, SYS_TIME + ($time), '/', self::domain());
            return $_COOKIE[$name];
        }
        return null;
    }

    /**
     * 获取server参数
     * @access public
     * @param string|array $name 数据名称
     * @param string $default 默认值
     * @return mixed
     */
    public static function server($name = '', $default = null)
    {
        !empty(self::$server) ?: self::$server = $_SERVER;
        if (is_array($name)) {
            return self::$server = array_merge(self::$server, $name);
        } elseif (!empty($name) && !empty($default)) {
            self::$server[$name] = $default;
        }
        return !empty($name) && array_key_exists($name, self::$server) ? self::$server[$name] : null;
    }

    /**
     * 获取上传的文件信息
     * @param string $name 名称
     */
    public static function file($name = '')
    {
        if (empty(self::$file) && isset($_FILES[$name])) {
            self::$file = $_FILES[$name];
        }
    }

    /**
     * 获取环境变量
     * @param string|array $name 数据名称
     * @return mixed
     */
    public static function env($name = '')
    {
        if (empty(self::$env)) {
            self::$env = $_ENV;
        }
        return self::$env[strtoupper($name)];
    }

    /**
     * 设置或者获取当前的Header
     * @access public
     * @param string|array $name header名称
     * @param string $default 默认值
     * @return string
     */
    public static function header($name = '', $default = null)
    {
        if (empty(self::$header)) {
            $header = [];
            if (function_exists('apache_request_headers') && $result = apache_request_headers()) {
                $header = $result;
            } else {
                $server = self::$server ?: $_SERVER;
                foreach ($server as $key => $val) {
                    if (0 === strpos($key, 'HTTP_')) {
                        $key = str_replace('_', '-', strtolower(substr($key, 5)));
                        $header[$key] = $val;
                    }
                }
                if (isset($server['CONTENT_TYPE'])) {
                    $header['content-type'] = $server['CONTENT_TYPE'];
                }
                if (isset($server['CONTENT_LENGTH'])) {
                    $header['content-length'] = $server['CONTENT_LENGTH'];
                }
            }
            self::$header = array_change_key_case($header);
        }
        if (is_array($name)) {
            return self::$header = array_merge(self::$header, $name);
        }
        if ('' === $name) {
            return self::$header;
        }
        $name = str_replace('_', '-', strtolower($name));
        return isset(self::$header[$name]) ? self::$header[$name] : $default;
    }

    /**
     * 当前是否ssl
     * @access public
     * @return bool
     */
    public static function isSsl()
    {
        $server = self::server();
        if (isset($server['HTTPS']) && ('1' == $server['HTTPS'] || 'on' == strtolower($server['HTTPS']))) {
            return true;
        } elseif (isset($server['REQUEST_SCHEME']) && 'https' == $server['REQUEST_SCHEME']) {
            return true;
        } elseif (isset($server['SERVER_PORT']) && ('443' == $server['SERVER_PORT'])) {
            return true;
        } elseif (isset($server['HTTP_X_FORWARDED_PROTO']) && 'https' == $server['HTTP_X_FORWARDED_PROTO']) {
            return true;
        }
        return false;
    }

    /**
     * 当前是否Ajax请求
     * @access public
     * @return bool
     */
    public static function isAjax()
    {
        $value = self::server('HTTP_X_REQUESTED_WITH');
        return ('xmlhttprequest' == $value) ? true : false;
    }

    /**
     * 当前是否Pjax请求
     * @access public
     * @return bool
     */
    public static function isPjax()
    {
        return !is_null(self::server('HTTP_X_PJAX')) ? true : false;
    }

    /**
     * 返回地址
     * @return null
     */
    function referer()
    {
        return isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
    }

    /**
     * 获取客户端IP地址
     * @param integer $ip_address 返回类型 0 返回IP地址 1 返回IPV4地址数字
     * @return mixed
     */
    public static function ip($ip_address = null)
    {
        if (is_numeric($ip_address) && $ip_address != 0) {
            return long2ip($ip_address);
        } elseif (is_null($ip_address)) {
            if (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
                $ip_address = getenv('HTTP_X_FORWARDED_FOR');
            } elseif (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
                $ip_address = getenv('HTTP_CLIENT_IP');
            } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
                $ip_address = getenv('REMOTE_ADDR');
            } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
                $ip_address = $_SERVER['REMOTE_ADDR'];
            }
            $ip_address = preg_match('/[\d\.]{7,15}/', $ip_address, $matches) ? $matches [0] : '127.0.0.1';
            return $ip_address;
        } elseif (Formats::isIP($ip_address)) {
            return ip2long($ip_address);
        }
        return null;
    }

    /**
     * 检测是否为内部IP
     * @param $ip
     * @return mixed
     */
    public static function isInsideIp($ip)
    {
        return in_array(strtok($ip, '.'), array('10', '127', '168', '192'));
    }

    /**
     * 检测是否使用手机访问
     * @access public
     * @return bool
     */
    public static function isMobile()
    {
        if (isset($_SERVER['HTTP_VIA']) && stristr($_SERVER['HTTP_VIA'], "wap")) {
            return true;
        } elseif (isset($_SERVER['HTTP_ACCEPT']) && strpos(strtoupper($_SERVER['HTTP_ACCEPT']), "VND.WAP.WML")) {
            return true;
        } elseif (isset($_SERVER['HTTP_X_WAP_PROFILE']) || isset($_SERVER['HTTP_PROFILE'])) {
            return true;
        } elseif (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/(blackberry|configuration\/cldc|hp |hp-|htc |htc_|htc-|iemobile|kindle|midp|mmp|motorola|mobile|nokia|opera mini|opera |Googlebot-Mobile|YahooSeeker\/M1A1-R2D2|android|iphone|ipod|mobi|palm|palmos|pocket|portalmmm|ppc;|smartphone|sonyericsson|sqh|spv|symbian|treo|up.browser|up.link|vodafone|windows ce|xda |xda_)/i', $_SERVER['HTTP_USER_AGENT'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取浏览器的内核名和版本号
     * @param string $is_browser_name 判断是否为指定的浏览器内核名
     * @return bool|string
     */
    public static function browserAndVer($is_browser_name = '')
    {
        $agent = self::server('HTTP_USER_AGENT');
        if (empty($agent)) {
            return '';
        }
        $browser = 'unknown';
        $browser_ver = '';
        // 包括 ie11 判断
        if (preg_match('/MSIE\s(\d+)\..*/i', $agent, $regs)) {
            $browser = "ie";
            $browser_ver = $regs[1];
        } elseif (preg_match('/FireFox\/(\d+)\..*/i', $agent, $regs)) {
            $browser = "firefox";
            $browser_ver = $regs[1];
        } elseif (preg_match('/Opera[\s|\/](\d+)\..*/i', $agent, $regs)) {
            $browser = 'opera';
            $browser_ver = $regs[1];
        } elseif (preg_match('/Chrome\/(\d+)\..*/i', $agent, $regs)) {
            $browser = "chrome";
            $browser_ver = $regs[1];
        } elseif ((strpos($agent, 'Chrome') == false) && preg_match('/Safari\/(\d+)\..*$/i', $agent, $regs)) {
            $browser = 'safari';
            $browser_ver = $regs[1];
        }
        if (!empty($is_browser_name)) {
            return Fileter::addslashes($browser) == $is_browser_name;
        }
        return Fileter::addslashes($browser . ' ' . $browser_ver);
    }

    /**
     * 当前URL地址中的scheme参数
     * @access public
     * @return string
     */
    public static function scheme()
    {
        return self::isSsl() ? 'https' : 'http';
    }

    /**
     * 当前请求URL地址中的query参数
     * @access public
     * @return string
     */
    public static function query()
    {
        return self::server('QUERY_STRING');
    }

    /**
     * 当前请求的host
     * @access public
     * @return string
     */
    public static function host()
    {
        return self::server('HTTP_HOST');
    }

    /**
     * 当前请求URL地址中的port参数
     * @access public
     * @return integer
     */
    public static function port()
    {
        return self::server('SERVER_PORT');
    }

    /**
     * 当前请求 SERVER_PROTOCOL
     * @access public
     * @return integer
     */
    public static function protocol()
    {
        return self::server('SERVER_PROTOCOL');
    }

    /**
     * 当前请求 REMOTE_PORT
     * @access public
     * @return integer
     */
    public static function remotePort()
    {
        return self::server('REMOTE_PORT');
    }

    /**
     * 当前请求 HTTP_CONTENT_TYPE
     * @access public
     * @return string
     */
    public static function contentType()
    {
        $contentType = self::server('CONTENT_TYPE');
        if ($contentType) {
            if (strpos($contentType, ';')) {
                list($type) = explode(';', $contentType);
            } else {
                $type = $contentType;
            }
            return trim($type);
        }
        return '';
    }

    /**
     * 获取当前请求的路由信息
     * @access public
     * @param string $route 路由名称
     * @return array
     */
    public static function routeInfo($route = '')
    {
        if (!empty($route)) {
            self::$routeInfo = explode("/", $route);
        }
        return self::$routeInfo;
    }

    /**
     * 设置或者获取当前请求的调度信息
     * @access public
     * @param array $dispatch 调度信息
     * @return array
     */
    public static function dispatch($dispatch = null)
    {
        if (!is_null($dispatch)) {
            self::$dispatch = $dispatch;
        }
        return self::$dispatch;
    }

    /**
     * 设置或者获取当前的模块名
     * @access public
     * @param string $module 模块名
     * @return string|Request
     */
    public static function module($module = null)
    {
        if (!is_null($module)) {
            self::$module = ucfirst($module);
        }
        return self::$module;
    }

    /**
     * 设置或者获取当前的控制器名
     * @access public
     * @param string $controller 控制器名
     * @return string|Request
     */
    public static function controller($controller = null)
    {
        if (!is_null($controller)) {
            self::$controller = ucfirst($controller);
        }
        return self::$controller;
    }

    /**
     * 设置或者获取当前的操作名
     * @access public
     * @param string $action 操作名
     * @return string|Request
     */
    public static function action($action = null)
    {
        if (!is_null($action)) {
            self::$action = $action;
        }
        return self::$action;
    }

    /**
     * 设置或者获取当前的语言
     * @access public
     * @param string $lang 语言名
     * @return string|Request
     */
    public static function langset($lang = null)
    {
        if (!is_null($lang)) {
            self::$langSet = $lang;
        }
        return self::$langSet ?: '';
    }
}