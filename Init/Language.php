<?php
/**
 * 中文语言包
 * User: wz_zh
 * Date: 2017/7/11
 * Time: 16:03
 */
$langCode = <<<PHP

<?php
const LANGUAGE = [
    500000=>[
        'zh-cn'=>'系统内部出错',
        'en'=>'System internal error',
    ],
    500101=>[
        'zh-cn'=>'系统远程通信出错，通信状态码：',
        'en'=>'System remote communication error, communication status code:',
    ],
    500102=>[
        'zh-cn'=>'系统远程通信出错，错误描述：',
        'en'=>'System remote communication error, Wrong description:',
    ],
    500103=>[
        'zh-cn'=>'CURL 没有找到请求方式：',
        'en'=>'CURL Not Request Mode:',
    ],
    500481=>[
        'zh-cn'=>'开始时间大于结束时间',
        'en'=>'Beginning time is longer than ending time',
    ],
];
PHP;
