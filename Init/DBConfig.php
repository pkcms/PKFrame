<?php
/**
 * save data parameters
 * User: Administrator
 * Date: 2018/12/21
 * Time: 16:19
 */

$DBConfig = <<<PHP
<?php
const DATABASE = [
    [
        'type'=>'mysql',
        'host'=>'',
        'port'=>3306,
        'name'=>'',
        'user'=>'',
        'pass'=>'',
        'character'=>'utf8',
        'prefix'=>'',
        'persistent'=>false,
    ],
];

const REDIS = [
    [
        'host'=>'',
        'port'=>6379,
        'setDB'=>0,
        'pass'=>'',
        'prefix'=>'',
    ],
];
PHP;
