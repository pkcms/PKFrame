<?php
/**
 * PK框架运行时遇到的消息提示.
 * User: wz_zh
 * Date: 2016/5/16
 * Time: 20:49
 */
return array(
    'system_file_noexists'=>array(
        'tips'=>'File or data loss in PK framework',
        'help_url'=>'',
    ),
    'template_file_noexists'=>array(
        'tips'=>'Sites Template file no exists:',
        'help_url'=>'',
    ),
);