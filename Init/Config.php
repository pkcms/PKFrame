<?php
/**
 * configuration parameters
 * User: Administrator
 * Date: 2018/12/24
 * Time: 11:13
 */
$Config = <<<PHP
<?php
// ================================ Necessary parameters of frame
const ROUTE_DEFAULT_MODULE = "";
const ROUTE_DEFAULT_CONTROLLER = "";
const RESULT_JSON_MODEL = [
    'resultField' => 'Result',
    'stateField' => 'State',
    'msgField' => 'Msg',
    'codeField'=> 'Code',
];
const DEBUG_MODE = true;
const IS_LOCAL_SESSION = true;
const SOAP_WSDL_CACHE_EXPIRATION = 0;
// ================================ Custom configuration parameters
PHP;
