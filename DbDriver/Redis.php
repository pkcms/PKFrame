<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/8/23
 * Time: 13:48
 */

namespace PKCore\DbDriver;

use function PKCore\fail;
use PKCore\Formats;

class Redis
{
    private static $_config;
    private static $_conn;
    private static $_index;
    private $_key;

    public function __construct($index = 0)
    {
        class_exists('Redis') ?: \PKCore\fail('php no exists redis');
        self::$_index = $index;
    }

    private function _init()
    {
        if (defined('REDIS') && is_array(REDIS) && array_key_exists(self::$_index, REDIS)) {
            $config = REDIS[self::$_index];
            self::$_config = $config;
            if (!isset($config['host']) || !isset($config['port']) || !isset($config['setDB'])) {
                \PKCore\fail('Redis Config Param (host | port | setDB) not Empty');
            }
            self::$_conn = new \Redis();
            self::$_conn->connect($config['host'], $config['port']);
            empty($config['pass']) ?: self::$_conn->auth($config['pass']);
            self::$_conn->select($config['setDB']);
            self::$_conn->ping() == '+PONG' ?: fail('redis Server is running');
        } else {
            fail('REDIS Config Param Is Empty');
        }
    }

    private function _ping()
    {
        if (!empty(self::$_conn) && method_exists(self::$_conn, 'ping')) {
            self::$_conn->ping() == '+PONG' ?: fail('redis Server is running');
        } else {
            $this->_init();
        }
    }

    public function Key($key)
    {
        $this->_init();
        Formats::isArray(self::$_config) && array_key_exists('prefix', self::$_config) ?: fail('Set key -> REDIS Config Param Error');
        $this->_key = self::$_config['prefix'] . $key;
        return $this;
    }

    /**
     * 保存数据到 Redis
     * @param null|string|array $value 值
     * @param null $ttl 时间以 指定时间格式 或 秒为单位
     */
    public function Set($value = null, $ttl = null)
    {
        $this->_ping();
        if (Formats::isArray($value) && method_exists(self::$_conn, 'lPush')) {
            foreach ($value as $item) {
                self::$_conn->lPush($this->_key, $item);
            }
        } elseif (method_exists(self::$_conn, 'set')) {
            self::$_conn->set($this->_key, $value);
        }
        if (method_exists(self::$_conn, 'expireAt') && !empty($ttl)
            && (Formats::pregMatch('date', $ttl) || Formats::pregMatch('datetime', $ttl))) {
            self::$_conn->expireAt($this->_key, strtotime($ttl));
        } elseif (method_exists(self::$_conn, 'expire') && $ttl > 0) {
            self::$_conn->expire($this->_key, $ttl);
        }
    }

    /**
     * 获取单个 key 的数据
     * @return bool|string
     */
    public function Get()
    {
        $this->_ping();
        if (method_exists(self::$_conn, 'get')) {
            return self::$_conn->get($this->_key);
        }
        return null;
    }

    /**
     * 判断 key 是否存在
     * @return bool
     */
    public function Has()
    {
        $this->_ping();
        if (method_exists(self::$_conn, 'exists')) {
            return self::$_conn->exists($this->_key);
        }
        return false;
    }

    /**
     * 获取（或者搜索）指定 key 的所有数据
     * @param $key
     * 查找符合给定模式的key。
     * KEYS *命中数据库中所有key
     * KEYS *o* 就会查找 key 中带有 o 字母的 key
     * KEYS h?llo命中hello， hallo and hxllo等
     * KEYS h*llo命中hllo和heeeeello等
     * KEYS h[ae]llo命中hello和hallo，但不命中hillo
     * @return array
     */
    public function Search($key)
    {
        $this->_ping();
        if (method_exists(self::$_conn, 'keys')) {
            return self::$_conn->keys($key);
        }
        return null;
    }

    /**
     * 可删除一个或者多个 Key
     */
    public function Del()
    {
        $this->_ping();
        if (method_exists(self::$_conn, 'del')) {
            self::$_conn->del($this->_key);
        }
    }

    public function Publish($key, $date)
    {
        $this->_ping();
        if (method_exists(self::$_conn, 'publish')) {
            self::$_conn->publish($key, $date);
        }
    }
    
}