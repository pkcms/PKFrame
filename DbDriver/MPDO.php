<?php
/**
 * POD 驱动
 * User: hui
 * Date: 2018/6/27
 * Time: 10:37
 */

namespace PKCore\DbDriver;

use PKCore\Log;
use PKCore\Request;
use function PKCore\fail;

class MPDO
{

    private $query;
    private static $config;
    private $dsn;
    private $_dbLinks, $_nowDBLinkIndex;

    public function __construct()
    {
        $this->_init();
    }

    private function _config()
    {
        $this->_link();
        if (defined('DATABASE') && is_array(DATABASE) && array_key_exists($this->_nowDBLinkIndex, DATABASE)) {
            self::$config = DATABASE[$this->_nowDBLinkIndex];
            $type = isset(self::$config['type']) ? self::$config['type'] : '';
            switch ($type) {
                case 'mysql':
                    isset(self::$config['host']) ?: fail('MYSql DataBase host address is Empty');
                    isset(self::$config['port']) ?: fail('MYSql DataBase port number is Empty');
                    isset(self::$config['name']) ?: fail('MYSql DataBase name is Empty');
                    isset(self::$config['user']) ?: fail('MYSql DataBase user is Empty');
                    isset(self::$config['pass']) ?: fail('MYSql DataBase pass is Empty');
                    break;
                case 'sqlite':
                    isset(self::$config['DBFile']) ?: fail('SQLite DataBase file is Empty');
                    break;
            }
        } else {
            fail('config param: DBLinkList is Empty Or not is array');
        }
    }

    private function _link()
    {
        $this->_nowDBLinkIndex = Request::param('NowDBLinkIndex');
        !is_null($this->_dbLinks) ?: ($this->_dbLinks = Request::has('MPDOLink') ? Request::param('MPDOLink') : []);
    }

    private function _init()
    {
        $this->_config();
        try {
            $type = isset(self::$config['type']) ? self::$config['type'] : '';
            $link_key = $type . $this->_nowDBLinkIndex;
            switch ($type) {
                case 'mysql':
                    $param = array(
                        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . (isset(self::$config['character']) ? self::$config['character'] : 'utf8'),
                        \PDO::ATTR_PERSISTENT => (isset(self::$config['persistent']) ? self::$config['persistent'] : false)
                    );
                    $this->dsn = $type . ':host=' . self::$config['host'] . ';port=' . self::$config['port']
                        . ';dbname=' . self::$config['name'];
                    $this->_dbLinks[$link_key] = new \PDO($this->dsn, self::$config['user'], self::$config['pass'], $param);
                    break;
                case 'sqlite':
                    $this->_dbLinks[$link_key] = new \PDO($type . ':' . PATH_ROOT . self::$config['DBFile']);
                    if (array_key_exists($link_key, $this->_dbLinks) && method_exists($this->_dbLinks[$link_key], 'exec')) {
                        $this->_dbLinks[$link_key]->exec('set names utf8');
                    }
                    break;
            }
            Request::param('MPDOLink', $this->_dbLinks);
        } catch (\PDOException  $e) {
            \PKCore\fail($this->dsn . ' -> PDOException:' . $e->getMessage());
        }
    }

    private function _ping()
    {
        try {
            $type = isset(self::$config['type']) ? self::$config['type'] : '';
            if (method_exists($this->_dbLinks[$type . $this->_nowDBLinkIndex], 'getAttribute')) {
                $this->_dbLinks[$type . $this->_nowDBLinkIndex]->getAttribute(\PDO::ATTR_SERVER_INFO);
            }
        } catch (\PDOException $e) {
            if (strpos($e->getMessage(), 'MySQL server has gone away') !== false) {
                return false;
            }
        }
        return true;
    }

    public function checkPing()
    {
        $this->_config();
        $type = isset(self::$config['type']) ? self::$config['type'] : '';
        switch ($type) {
            case 'mysql':
                if (!array_key_exists($type . $this->_nowDBLinkIndex, $this->_dbLinks)) {
                    return false;
                }
                if (!$this->_ping()) {
                    $this->_init();
                }
                break;
        }
        return true;
    }

    public function sqlQuery($sql)
    {
        if (!$this->checkPing()) {
            return false;
        }
        $link_key = (isset(self::$config['type']) ? self::$config['type'] : '') . $this->_nowDBLinkIndex;
        $start_time = microtime(true);
        if (strstr(strtolower($sql), "update")) {
            if (method_exists($this->_dbLinks[$link_key], 'prepare')) {
                $this->query = $this->_dbLinks[$link_key]->prepare($sql);
            }
            if (method_exists($this->query, 'execute')) {
                $result = $this->query->execute();
                $result ?: \PKCore\fail(array('query' => $sql, 'error' =>
                    method_exists($this->_dbLinks[$link_key], 'errorInfo') ? $this->_dbLinks[$link_key]->errorInfo() : ''));
            }
//            var_dump($this->query->rowCount());
        } else {
            if (strstr(strtolower($sql), "select")) {
                if (method_exists($this->_dbLinks[$link_key], 'query')) {
                    $this->query = $this->_dbLinks[$link_key]->query($sql);
                }
            } else {
                if (method_exists($this->_dbLinks[$link_key], 'exec')) {
                    $this->query = $this->_dbLinks[$link_key]->exec($sql);
                }
            }
//            var_dump($this->_dbLinks[$link_key]->errorCode() == '00000');
            if (method_exists($this->_dbLinks[$link_key], 'errorCode')
                && in_array($this->_dbLinks[$link_key]->errorCode(), ['00000'])) {
            } else {
                \PKCore\fail(array('query' => $sql, 'error' =>
                    method_exists($this->_dbLinks[$link_key], 'errorInfo') ? $this->_dbLinks[$link_key]->errorInfo() : ''));
            }
        }
        $doTime = \PKCore\executeTime($start_time);
        $doTime < 1 ?: Log::LOGS('sql', ['sql' => $sql, 'time' => $doTime]);
        return true;
    }

    public function fetchAssoc($sql, $toList = false)
    {
        if (!$this->sqlQuery($sql)) {
            return false;
        }
        if (method_exists($this->query, 'setFetchMode')) {
            $this->query->setFetchMode(\PDO::FETCH_ASSOC);
        }
        if ($toList) {
            if (method_exists($this->query, 'fetchAll')) {
                return $this->query->fetchAll();
            }
        } else {
            if (method_exists($this->query, 'fetch')) {
                return $this->query->fetch();
            }
        }
        return null;
    }

    function insertId($sql)
    {
        $this->sqlQuery($sql);
        $link_key = (isset(self::$config['type']) ? self::$config['type'] : '') . $this->_nowDBLinkIndex;
        if (is_array($this->_dbLinks) && array_key_exists($link_key, $this->_dbLinks)
            && method_exists($this->_dbLinks[$link_key], 'lastInsertId')) {
            return $this->_dbLinks[$link_key]->lastInsertId();
        }
        return null;
    }

}