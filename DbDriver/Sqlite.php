<?php

namespace PKCore\DbDriver;

use function PKCore\fail;
use function PKCore\Route\language;

class Sqlite
{

    private static $conn = null;
    private static $_dbFile;

    public function __construct()
    {
        extension_loaded('sqlite3') ?: exit(language('no_sqlite3_extension'));
        defined('PKCore\SqliteConfig\FILE') ?: die('no sqlite config info -> file ');
    }

    /**
     * @param mixed $dbFile
     */
    public function setDbFile($dbFile)
    {
        self::$_dbFile = $dbFile;
        self::$conn = new \SQLite3(PATH_ROOT . $dbFile);
    }

    private function sqlite3Error($sql)
    {
        if (method_exists(self::$conn, 'lastErrorMsg')
            && method_exists(self::$conn, 'lastErrorCode')) {
            fail(array('query' => $sql,
                'errorMessage' => self::$conn->lastErrorMsg(),
                'errorCode' => self::$conn->lastErrorCode()));
        }
    }

    public function sqlQuery($sql)
    {
        if (method_exists(self::$conn, 'busyTimeout')
            && method_exists(self::$conn, 'exec')
            && method_exists(self::$conn, 'lastErrorCode')) {
            self::$conn->busyTimeout(10);
            self::$conn->exec($sql);
            if (self::$conn->lastErrorCode() != 0) {
                $this->sqlite3Error($sql);
            }
        }
    }

    /**
     * 查询语句
     * @param $sql
     * @return array
     */
    public function fetchAssoc($sql)
    {
        $result = array();
        if (method_exists(self::$conn, 'query')
            && method_exists(self::$conn, 'lastErrorCode')) {
            $sth = self::$conn->query($sql);
            if (self::$conn->lastErrorCode() != 0) {
                $this->sqlite3Error($sql);
            }
            if (method_exists($sth, 'fetchArray')) {
                while ($row = $sth->fetchArray()) {
                    $result[] = $row;
                }
            }
        }
        return $result;
    }

    /**
     * 向数据表添加数据
     * @param $sql
     * @return int
     */
    function insertId($sql)
    {
        $result = null;
        if (method_exists(self::$conn, 'lastInsertRowID')) {
            $this->sqlQuery($sql);
            $result = self::$conn->lastInsertRowID();
        }
        return $result;
    }

    /**
     * 析构函数，自动关闭数据库,垃圾回收机制
     */
    public function __destruct()
    {
        if (method_exists(self::$conn, 'close')) {
            self::$conn->close();
        }
    }

}
