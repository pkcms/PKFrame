<?php
/**
 * Mysql
 * User: 张辉
 * Date: 2016/9/3
 * Time: 17:05
 * -----------------------------------------------------------------------------
 *  历史更新纪录
 * -----------------------------------------------------------------------------
 * 修改内容：
 * 修改人：
 * 修改时间：
 * -----------------------------------------------------------------------------
 */

namespace PKCore\DbDriver;

use PKCore\Log;
use PKCore\Request;
use function PKCore\fail;

class Mysql
{

    private $host, $query;
    private static $config;
    private $_dbLinks, $_nowDBLinkIndex;

    public function __construct()
    {
        $this->_init();
    }

    private function _config()
    {
        $this->_link();
        if (defined('DATABASE') && is_array(DATABASE) && array_key_exists($this->_nowDBLinkIndex, DATABASE)) {
            self::$config = DATABASE[$this->_nowDBLinkIndex];
            $type = isset(self::$config['type']) ? self::$config['type'] : '';
            switch ($type) {
                case 'mysql':
                    isset(self::$config['host']) ?: fail('MYSql DataBase host address is Empty');
                    isset(self::$config['port']) ?: fail('MYSql DataBase port number is Empty');
                    isset(self::$config['name']) ?: fail('MYSql DataBase name is Empty');
                    isset(self::$config['user']) ?: fail('MYSql DataBase user is Empty');
                    isset(self::$config['pass']) ?: fail('MYSql DataBase pass is Empty');
                    break;
                case 'sqlite':
                    isset(self::$config['DBFile']) ?: fail('SQLite DataBase file is Empty');
                    break;
            }
        } else {
            fail('config param: DBLinkList is Empty Or not is array');
        }
    }

    private function _link()
    {
        $this->_nowDBLinkIndex = Request::param('NowDBLinkIndex');
        !is_null($this->_dbLinks) ?: ($this->_dbLinks = Request::has('DBLink') ? Request::param('DBLink') : []);
    }

    private function _init()
    {
        $this->_config();
        $link_key = (isset(self::$config['type']) ? self::$config['type'] : '') . $this->_nowDBLinkIndex;
        $character = isset(self::$config['character']) ? self::$config['character'] : 'utf8';
        $persistent = isset(self::$config['persistent']) ? self::$config['persistent'] : false;
        $this->host = ($persistent ? 'p:' : '') . self::$config['host'];
        // 处理不带端口号的socket连接情况
        $this->_dbLinks[$link_key] = @mysqli_connect($this->host, self::$config['user'], self::$config['pass'],
            self::$config['name'], self::$config['port']);
        mysqli_set_charset($this->_dbLinks[$link_key], $character);
        Request::param('DBLink', $this->_dbLinks);
    }

    public function checkPing()
    {
        $this->_config();
        $link_key = (isset(self::$config['type']) ? self::$config['type'] : '') . $this->_nowDBLinkIndex;
        if (!array_key_exists($link_key, $this->_dbLinks)) {
            // 没有建立过
            $this->_init();
            $link_id = $this->_dbLinks[$link_key];
        } else {
            // 建立过
            $link_id = $this->_dbLinks[$link_key];
            if (!mysqli_ping($link_id)) {
                // 但已经断开了
                $this->_init();
                $link_id = $this->_dbLinks[$link_key];
            }
        }
        return $link_id;
    }

    public function sqlQuery($sql)
    {
        $link_id = $this->checkPing();
        $start_time = microtime(true);
        // 判断是否开启运行时间开关
        if (strpos($sql, ';')) {
            $this->query = mysqli_multi_query($link_id, $sql);
        } else {
            $this->query = mysqli_query($link_id, $sql);
        }
        $this->query ?: \PKCore\fail(array('query' => $sql, 'error' => mysqli_error($link_id)));
        $v_time = \PKCore\executeTime($start_time);
        $v_time < 1 ?: Log::LOGS('do sql', ['sql'=>$sql, 'time' => $v_time]);
        return true;
    }

    public function fetchAssoc($sql)
    {
        $link_key = (isset(self::$config['type']) ? self::$config['type'] : '') . $this->_nowDBLinkIndex;
        if (!$this->sqlQuery($sql)) {
            return false;
        }
        if (strpos($sql, ';')) {
            $result = array();
            $i = 0;
            do {
                //获取第一个结果集
                if ($res = mysqli_store_result($this->_dbLinks[$link_key])) {
                    if ($res->num_rows < 2) {
                        $result[$i] = mysqli_fetch_assoc($res);
                    } else {
                        $result[$i] = array();
                        while ($row = mysqli_fetch_assoc($res)) {
                            $result[$i][] = $row;
                        }
                    }
                    $i++;
                }
            } while (mysqli_next_result($this->_dbLinks[$link_key]));
        } elseif (strstr(strtolower($sql), "order")) {
            $array = array();
            while ($row = mysqli_fetch_assoc($this->query)) {
                $array[] = $row;
            }
            return $array;
        } else {
            $result = mysqli_fetch_assoc($this->query);
        }
        return $result;
    }

    function insertId($sql)
    {
        $this->sqlQuery($sql);
        $link_key = (isset(self::$config['type']) ? self::$config['type'] : '') . $this->_nowDBLinkIndex;
        return mysqli_insert_id($this->_dbLinks[$link_key]);
    }

}