<?php
/**
 * 文件处理
 * User: wz_zh
 * Date: 2017/7/27
 * Time: 13:59
 */

namespace PKCore;

class Files
{

    /**
     * 检查文件中是否带有BOM
     * @param string $filename 被检查的文件名
     */
    public static function isBom($filename = '')
    {
        $contents = file_get_contents($filename);
        $charset[1] = substr($contents, 0, 1);
        $charset[2] = substr($contents, 1, 1);
        $charset[3] = substr($contents, 2, 1);
        if (ord($charset[1]) == 239 && ord($charset[2]) == 187 && ord($charset[3]) == 191) {
            $data = substr($contents, 3);
            $filenum = fopen($filename, "w");
            flock($filenum, LOCK_EX);
            fwrite($filenum, $data);
            fclose($filenum);
        }
    }

    /**
     * 检查要保存的文件名
     *
     * @param string $str
     * @return string
     */
    public static function checkFileName($str = '')
    {
        $str = str_replace(' ', '-', $str);
        $str = str_replace('/', '-', $str);
        $str = str_replace(',', '', $str);
        $str = str_replace('&nbsp;', '', $str);
        $str = str_replace(';', '', $str);
        $str = str_replace('&', '', $str);
        $str = urlencode($str);
//        $str = strtolower($str);
        $str = str_replace('%26nbsp%3b', '', $str);
        $str = str_replace('%c2%a0', '', $str);
        $str = str_replace('%3a', ':', $str);
        return $str;
    }

    /**
     * 获取文件（属性）信息，包含基本属性
     * @param string $path 文件所在位置
     * @return array|mixed
     */
    public static function property($path = '')
    {
        $header_info = stristr($path, 'http://') ?
            (!Formats::isUrl($path) ? $path : get_headers($path))
            : $header_info = file_exists($path) ? self::baseProperty($path)
                : $path;
        if (Formats::isArray($header_info) == FALSE) {
            return [];
        }
        $info = pathinfo($path);
        $img_type = ['jpg', 'gif', 'png'];
        if (in_array($info['extension'], $img_type)) {
            $info = array_merge($info, getimagesize($path));
        }
        if (isset($header_info)) {
            $info = array_merge($info, $header_info);
            $info['size'] = filesize($path);
        }
        if (isset($arr) && Formats::isArray($arr))
            $info = array_merge($info, $arr);
        return $info;
    }

    /**
     * 文件基本属性
     * a_time：上次访问时间，c_time：文件创建时间，m_time：上次修改时间
     * perms：文件权限，size：文件大小，type：文件类型
     * @param string $path
     * @return array
     */
    public static function baseProperty($path = '')
    {
        $result = [];
        if (!file_exists($path)) {
            return $result;
        }
        $result['a_time'] = fileatime($path);
        $result['c_time'] = filectime($path);
        $result['m_time'] = filemtime($path);
        $result['perms'] = substr(sprintf("%o", fileperms($path)), -4);
        $result['size'] = filesize($path);
        $result['type'] = filetype($path);
        return $result;
    }

    /**
     * 文件的保存
     * @param string $path 保存路径
     * @param string $fileName 保存文件名
     * @param string $data 保存内容的信息
     * @param bool $isTop true：头部开始，false：尾部开始
     */
    public static function putContents($path = '', $fileName = '', $data = null, $isTop = true)
    {
        $fileName = self::checkFileName($fileName);
        $path = rtrim($path, DS) . DS;
        self::mkdir($path);
        if (file_exists($path . $fileName)) {
            $info = self::property($path . $fileName);
            if ($info['perms'] == 444) {
                $old = umask(0);
                chmod($path . $fileName, 0755);
                umask($old);
            }
        }
        $data = iconv("UTF-8", "UTF-8", $data);
        if ($isTop) {
            file_put_contents($path . $fileName, $data);
        } else {
            file_put_contents($path . $fileName, $data, FILE_APPEND);
        }
    }

    /**
     * 文件的读取
     * @param $fileName
     * @param int $HTTP_timeOut HTTP 协议读取的超时设定，0为不设定
     * @param bool $is_remove_HP false：普通读取，true：过滤掉 HTML 和 PHP 标记
     * @return bool|null|string
     */
    public static function getContents($fileName, $HTTP_timeOut = 0, $is_remove_HP = false)
    {
        $buffer = null;
        if (!strpos($fileName, '://') && !file_exists($fileName)) {
            return $buffer;
        }
        !file_exists($fileName) ?: (is_readable($fileName) ?: die('file unable read ' . $fileName));
        if (!$is_remove_HP && function_exists('file_get_contents')) {
            if (strpos($fileName, '://')) {
                if (!get_cfg_var('allow_url_fopen')) {
                    return false;
                }
                if ($HTTP_timeOut > 0) {
                    $context = stream_context_create(
                        array('http' => array('timeout' => $HTTP_timeOut))
                    );
                    $buffer = @file_get_contents($fileName, false, $context);
                } else {
                    $buffer = @file_get_contents($fileName, false);
                }
            } else {
                $buffer = file_get_contents($fileName);
            }
        } else {
            $fp = \fopen($fileName, 'r');
            if ($is_remove_HP) {
                while (!feof($fp)) {
                    $buffer = fgetss($fp, 4096);
                }
            } else {
                while (!feof($fp)) {
                    $buffer = fgets($fp, 4096);
                }
            }
            fclose($fp);
        }
        return $buffer;
    }

    /**
     * 保存上传文件
     * @param string $tmp 缓冲文件
     * @param string $path 保存文件的路径
     * @param string $new 保存文件的新文件名
     */
    public static function uploaded($tmp, $path, $new)
    {
        $new = self::checkFileName($new);
        self::mkdir($path);
        set_time_limit(0);
        \move_uploaded_file($tmp, $path . $new) ?: die('uploaded file fail');
    }

    /**
     * 文件下载
     * @param string $filepath 文件路径
     */
    public static function download($filepath = '')
    {
        $filename = basename($filepath);
        !strpos(Request::browserAndVer(), 'ie') ?: $filename = rawurlencode($filename);
        $filetype = strtolower(trim(substr(strrchr($filename, '.'), 1, 10)));
        $filesize = sprintf("%u", filesize($filepath));
        if (ob_get_length() !== false)
            @ob_end_clean();
        header('Pragma: public');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Tools-Control: no-store, no-cache, must-revalidate');
        header('Tools-Control: pre-check=0, post-check=0, max-age=0');
        header('Content-Transfer-Encoding: binary');
        header('Content-Encoding: none');
        header('Content-type: ' . $filetype);
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-length: ' . $filesize);
        readfile($filepath);
        exit;
    }

    /**
     * 文件复制
     * @param string $source 原文件路径（包括文件所在路径和文件名）
     * @param string $target 要复制到目的的路径
     * @param string $file_name 要复制目的的文件名
     * @return boolean
     */
    public static function copy($source = '', $target = '', $file_name = '')
    {
        if (file_exists($source)) {
            file_exists($target) ?: self::mkdir($target);
            $file_name = self::checkFileName($file_name);
            copy($source, $target . '/' . $file_name);
            return true;
        } else
            return false;
    }

    /**
     * 目录（文件夹）的创建
     * @param string $path 目录（文件夹）的路径
     * @return boolean
     */
    public static function mkdir($path)
    {
        if (empty($path)) {
            return $path;
        }
        $path = str_replace('\\', DS, $path);
        $paths = explode(DS, $path);
        $result = array_shift($paths);
        foreach ($paths as $value) {
            $result .= DS . self::checkFileName($value);
            try {
                file_exists($result) || !strstr($result, PATH_ROOT) ?: (@\mkdir($result, 0755) ?: fail('mkdir error: ' . $result));
            } catch (\Exception $e) {
                fail('mkdir:' . $result . 'errMsg:' . $e->getMessage());
            }
        }
        return $result . DS;
    }

    /**
     * 目录（文件夹）的清空
     * @param string $path 目录（文件夹）的路径
     * @param array $exclude_dir 排除的文件夹
     */
    public static function remove($path = '', $exclude_dir = array())
    {
        if (file_exists($path)) {
            if ($dh = @\opendir($path)) {
                while ($fstr = @\readdir($dh)) {
                    if ($fstr == "." || $fstr == "..") {
                        continue;
                    }
                    $fname = rtrim($path, DS) . DS . $fstr;
                    if (is_dir($fname)) {
                        if (Formats::isArray($exclude_dir) && in_array($fstr, $exclude_dir)) {
                            continue;
                        }
                        \is_writable($fname) ?: die($fname . 'not writable');
                        \chmod($fname, 0666);
                        self::unlink($fname);
                    } else {
                        if (file_exists($fname)) {
                            self::unlink($fname);
                        }
                    }
                }
            }
        } elseif (file_exists($path)) {
            self::unlink($path);
        }
    }

    /**
     * 系统内置删除，可解决 windows 下因权限问题而不能删除的情况
     * @param $fileName
     */
    public static function unlink($fileName)
    {
        $windows_server = strpos($fileName, '/', 0) !== false ? false : true;
        $deleteError = 0;
        if (!$windows_server) {
            if (!\unlink($fileName)) {
                $deleteError = 1;
            }
        } else {
            $lines = array();
            exec("DEL /F/Q \"$fileName\"", $lines, $deleteError);
        }
        if ($deleteError) {
            die('file delete error');
        }
    }

    /**
     * 目录（文件夹）的浏览
     * @param string $dir 目录（文件夹）
     * @param string $file_name 查找的文件名
     * @return array|bool
     */
    public static function opendir($dir = '', $file_name = '')
    {
        $result = array();
        if (stristr($dir, 'ftp://')) {
            $headdir = @\opendir($dir) or die("Cannot open " . $dir);
            while (($file = opendir($headdir)) !== false) {
                $result[] = $file;
            }
            closedir($headdir);
        } else {
            $result = self::dirChildren($dir, $file_name);
        }
        return $result;
    }

    /**
     * 遍历子目录
     * @param string $path 要遍历的目录
     * @param string $file_name 要查找的文件名
     * @return array|bool 如果有返回数组，否则返回FALSE
     */
    private static function dirChildren($path = '', $file_name = '')
    {
        $headdir = @dir($path);
        $result = [];
        if (!method_exists($headdir, 'read')) {
            return $result;
        }
        while (($file = $headdir->read()) != FALSE) {
            if (strpos($file, '.') > 0) {
                if ($file_name) {
                    if (stristr($file, $file_name) && stripos($file, $file_name) == 0) {
                        $result[] = $file;
                    }
                } else
                    $result[] = $file;
            }
            if (!stristr($file, '.')) {
                $lists = self::dirChildren($path . '/' . $file, $file_name);
                if (!empty($lists))
                    $result[$file] = $lists;
            }
        }
        $headdir->close();
        return $result;
    }

}