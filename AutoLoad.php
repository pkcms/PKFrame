<?php
/**
 * 自动加载
 * User: Administrator
 * Date: 2019/5/24
 * Time: 9:53
 */

use function PKCore\Route\isLoadingFile;


$func_file = PATH_PK . 'Func.php';
file_exists($func_file) ?: die('func php no exists');
/** @noinspection PhpIncludeInspection */
require_once "{$func_file}";

$route_file = PATH_PK . 'Route.php';
file_exists($route_file) ?: die('router drive no exists');
/** @noinspection PhpIncludeInspection */
require_once "{$route_file}";

/**
 * @param $name
 */
function autoLoad($name)
{
    $nameArr = explode('\\', $name);
//    print_r($nameArr);
    if (is_array($nameArr) && stristr($nameArr[0], 'PK')) {
        switch ($nameArr[0]) {
            case 'PKCore':
                if (count($nameArr) == 2) {
                    PKCore\Route\isLoadingFile(PATH_PK . $nameArr[1]);
                } else {
                    print_r($nameArr);
                    exit();
                }
                break;
            case 'PKController':
                isLoadingFile(PATH_ROOT . $name) ?: \PKCore\fail('not loading controller file: ' . $name);
                break;
            case 'PKExtend':
                count($nameArr) != 2 ?: PKCore\Route\extend($nameArr[1]);
                break;
            case 'PKModel':
                count($nameArr) != 2 ?: PKCore\Route\model($nameArr[1]);
                break;
            case 'PKStorage':
                count($nameArr) != 2 ?: PKCore\Route\storage($nameArr[1]);
                break;
            default:
                print_r($nameArr);
                exit();
                break;
        }
    }
}

spl_autoload_register('autoLoad');
