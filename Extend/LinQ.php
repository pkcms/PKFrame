<?php
/**
 * 加载 LinQ
 * User: wz_zh
 * Date: 2018/8/7
 * Time: 20:00
 */

$extendPath = __DIR__ . DS . 'LinQ' . DS;
\PKCore\Route\isLoadingFile($extendPath . 'factory' . DS . 'JoinFactory.php');
\PKCore\Route\isLoadingFile($extendPath . 'helper' . DS . 'IJoinHelper.php');
\PKCore\Route\isLoadingFile($extendPath . 'helper' . DS . 'JoinHelper.php');
\PKCore\Route\isLoadingFile($extendPath . 'helper' . DS . 'LeftJoinHelper.php');
\PKCore\Route\isLoadingFile($extendPath . 'Linq.php');
\PKCore\Route\isLoadingFile($extendPath . 'JsonLinq.php');
\PKCore\Route\isLoadingFile($extendPath . 'LinqFactory.php');
\PKCore\Route\isLoadingFile($extendPath . 'XmlLinq.php');