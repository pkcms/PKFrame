<?php
/**
 * 内存
 * User: wz_zh
 * Date: 2017/7/13
 * Time: 15:46
 */

namespace PKCore;

use PKCore\DbDriver\Redis;

Route\DbDriver('redis');

class RedisAction
{
    /** @var Redis */
    private static $_class;

    public function __construct($host = '', $port = '', $pass = '', $selectDB = 0)
    {
        self::$_class = new Redis($host, $port, $pass, $selectDB);
    }

    public static function set($data = null, $name = '', $expire = 0)
    {
        self::$_class->set($name, $data, $expire);
    }

    public static function get($name = '', $type = '')
    {
        return method_exists(self::$_class, 'getAlone') ?
            self::$_class->getAlone($type . '_' . $name) : null;
    }

    public static function del($name = '', $type = '')
    {
        !method_exists(self::$_class, 'delKey') ?: self::$_class->delKey($type . '_' . $name);
    }

}