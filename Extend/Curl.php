<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/12/29
 * Time: 14:00
 */

namespace PKCore\Extend;

use PKCore\Route;
use PKCore\Converter;

class Curl
{
    private $_url;
    private $_isSSI;
    private $_param;
    private $_method;
    private $_timeOut = 0;
    private $_init;
    private $_response;
    private $_isSetCookie = false;

    public function __construct($url, $method)
    {
        function_exists('curl_init') or \PKCore\fail('php extend curl no exists');
        $this->_url = $url;
        $this->_isSSI = stristr($url, 'https');
        $this->_method = $method;
    }

    public function Param($param = [])
    {
        switch (strtolower($this->_method)) {
            case 'get':
            case 'file':
                $this->_url .= '?' . Converter::arrayToJoinString($param);
                break;
            case 'post':
                $this->_param = Converter::arrayToJoinString($param);
                break;
            case 'json':
                $this->_param = Converter::jsonEnCode($param);
                break;
        }
        return $this;
    }

    public function SetCookie()
    {
        $this->_isSetCookie = true;
        return $this;
    }

    /**
     * 超时
     * @param int $second
     * @return $this
     */
    public function TimeOut($second = 0)
    {
        $this->_timeOut = $second;
        return $this;
    }

    public function OutPutParam()
    {
        exit(var_dump($this));
    }

    public function Request()
    {
        $this->_init = curl_init();
        // 设置超时限制防止死循环
        if ($this->_timeOut > 0) {
            curl_setopt($this->_init, CURLOPT_TIMEOUT, $this->_timeOut);
            curl_setopt($this->_init, CURLOPT_CONNECTTIMEOUT, $this->_timeOut);
        }
        $actionName = 'request' . ucfirst($this->_method);
        if (method_exists($this, $actionName)) {
            if (version_compare(phpversion(), '7.0.0', '>=')) {
                $this->{$actionName}();
            } else {
                $this->$actionName();
            }
        } else {
            \PKCore\fail(Route\language(500103) . $this->_method);
        }
        if ($actionName != 'requestFILE') {
            curl_setopt($this->_init, CURLOPT_CUSTOMREQUEST, strtoupper($this->_method));
            curl_setopt($this->_init, CURLOPT_URL, $this->_url);
            $this->_multi();
        }
        return $this->_response;
    }

    private function _common()
    {
        curl_setopt($this->_init, CURLOPT_HEADER, $this->_isSetCookie);
        curl_setopt($this->_init, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($this->_init, CURLOPT_FAILONERROR, true);
    }

    protected function requestGET()
    {
        $this->_common();
        if ($this->_isSSI) {
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYHOST, FALSE);
        }
    }

    protected function requestPOST()
    {
        if ($this->_isSSI) {
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYHOST, FALSE);
            //CURL_SSLVERSION_TLSv1
            curl_setopt($this->_init, CURLOPT_SSLVERSION, TRUE);
        }
        self::_common();
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            curl_setopt($this->_init, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        }
        curl_setopt($this->_init, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->_init, CURLOPT_AUTOREFERER, 1);
        !array_key_exists('cookie_file', $GLOBALS) ?:
            curl_setopt($this->_init, CURLOPT_COOKIEFILE, $GLOBALS['cookie_file']);
        curl_setopt($this->_init, CURLOPT_POST, true);
        curl_setopt($this->_init, CURLOPT_POSTFIELDS, $this->_param);
    }

    protected function requestJSON()
    {
        if ($this->_isSSI) {
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($this->_init, CURLOPT_SSL_VERIFYHOST, FALSE);
            //CURL_SSLVERSION_TLSv1
            curl_setopt($this->_init, CURLOPT_SSLVERSION, TRUE);
        }
        $this->_common();
        curl_setopt($this->_init, CURLOPT_POST, true);
        curl_setopt($this->_init, CURLOPT_POSTFIELDS, $this->_param);
        curl_setopt($this->_init, CURLOPT_VERBOSE, 1);
        curl_setopt($this->_init, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->_init, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($this->_param)
            )
        );
    }

    protected function requestFILE()
    {
        curl_setopt($this->_init, CURLOPT_URL, $this->_url);
        curl_setopt($this->_init, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->_init, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->_init, CURLOPT_RETURNTRANSFER, 1);
        $this->_response = curl_exec($this->_init); // 已经获取到内容，没有输出到页面上。
        curl_close($this->_init);
    }

    /**
     * 只支持 5.5.0 以上的版本(批处理)
     */
    private function _multi()
    {
        $handle = curl_multi_init();
        curl_multi_add_handle($handle, $this->_init);
        $flag = null;
        do {
            $status = curl_multi_exec($handle, $flag);
            // Check for errors
            if ($status > 0) {
                // Display error message
                \PKCore\fail(Route\language(500102) . curl_multi_strerror($status));
                break;
            }
        } while ($flag > 0);
        $http_code = curl_getinfo($this->_init, CURLINFO_HTTP_CODE);
        if ($http_code != 200) {
            \PKCore\fail(Route\language(500101) . $http_code);
        }
        $this->_response = curl_multi_getcontent($this->_init);
        curl_multi_remove_handle($handle, $this->_init);
        curl_multi_close($handle);
    }

}
