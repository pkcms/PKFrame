<?php

/**
 * PK框架 - 底层 路由
 * User: zhanghui - qq:366065186
 * Date: 2016/4/5
 * Time: 12:27
 */

namespace PKCore\Route;

use function PKCore\handlerException;
use PKCore\Log;
use PKCore\Statics;
use PKCore\Request;

const FILE_EXT = '.php';

function gotoApps()
{
    Log::LOGS('request info',
        ['request_url' => Request::url(), 'method' => Request::method(), 'post' => Request::post()]);
    Request::route();
    $dir = PATH_ROOT . 'PKController' . DS;
    $module_name = Request::module();
    $ctrl_name = Request::controller();
    $ctrl_file_name = ucfirst($module_name) . DS . ucfirst($ctrl_name);
//    \PKCore\Route\config('autoLoad');
    $class_name = '\PKController\\' . $module_name . '\\' . $ctrl_name;
    class_exists($class_name) ?: \PKCore\fail('not ' . $ctrl_name . ' controller');
    if (isset($_SERVER['QUERY_STRING']) && strtolower($_SERVER['QUERY_STRING']) == 'wsdl' && Request::action() == 'main') {
        \PKCore\Extend\SOAPServerCreateWSDL($dir, $ctrl_file_name, $class_name);
    } else {
        $ctrl = new $class_name();
        $action = Request::action();
        is_callable(array($ctrl, $action)) ?: \PKCore\fail('controller no action ' . $action);
        $result = $ctrl->$action();
        if (isset($_SERVER['RUN_NODE']) && strtolower($_SERVER['RUN_NODE']) == 'cmd') {
        } elseif (Request::isAjax()) {
            Statics::resultJsonModel($result);
        }
    }
}

/**
 * 加载配置文件
 * @param $file_name
 * @return mixed
 */
function config($file_name)
{
    $dir = PATH_ROOT . 'PKConfig' . DS;
    $file_name = ucfirst($file_name);
    return isLoadingFile($dir . $file_name);
}

/**
 * 加载MODEL
 * @param $module_name - 模块名
 */
function model($module_name)
{
    try {
        strpos($module_name, 'Model') ?: $module_name .= 'Model';
        $path = PATH_ROOT . 'PKModel' . DS . ucfirst($module_name);
        if (!isLoadingFile($path)) {
            throw new \Exception('no loading file:' . $path);
        }
    } catch (\Exception $exception) {
        handlerException($exception);
    }
}

/**
 * 存储层
 * @param $module_name
 */
function storage($module_name)
{
    try {
        config('DBConfig');
        $path = PATH_ROOT . 'PKStorage' . DS . ucfirst($module_name);
        if (!isLoadingFile($path)) {
            throw new \Exception('no loading file:' . $path);
        }
    } catch (\Exception $exception) {
        handlerException($exception);
    }
}

/**
 * 加载扩展
 * @param $extend_name - 扩展的文件名
 * @param bool $is_core
 */
function extend($extend_name, $is_core = false)
{
    try {
        $path = ($is_core ? PATH_PK . DS . 'Extend' : PATH_ROOT . 'PKExtend') . DS . ucfirst($extend_name);
        if (!isLoadingFile($path)) {
            throw new \Exception('no loading file:' . $path);
        }
    } catch (\Exception $exception) {
        handlerException($exception);
    }
}

/**
 * 加载 .phar 包
 * @param $phar_name - 包名
 */
function phar($phar_name)
{
    try {
        strpos($phar_name, '.') ?: $phar_name .= '.phar';
        $path = PATH_ROOT . 'PKPhar' . DS . ucfirst($phar_name);
        if (!isLoadingFile($path)) {
            throw new \Exception('no loading file:' . $path);
        }
    } catch (\Exception $exception) {
        handlerException($exception);
    }
}

/**
 * 加载系统文件
 * @param $file_name
 */
function DbDriver($file_name)
{
    $dir = PATH_PK . 'DbDriver' . DS;
    isLoadingFile($dir . ucfirst($file_name)) ?: \PKCore\fail('no loading ' . $dir . ucfirst($file_name) . ' driver');
}

/**
 * 语言文件处理
 * @param $code
 * @param string $app (取决于应用区域，如：system)
 * @param string $languageType (语言的类型，如： zh-cn、en)
 * @return mixed|string
 */
function language($code, $app = 'system', $languageType = 'zh-cn')
{
    $is_loading = config('Language');
    if ($is_loading && defined('LANGUAGE') && array_key_exists($code, LANGUAGE)) {
        $langArr = LANGUAGE[$code];
        if (is_array($langArr) && array_key_exists($languageType, $langArr)) {
            return $langArr[$languageType];
        }
    }
    if ($app != 'system') {
        $appLanguage = 'Language_' . $app;
        $is_loading = config($appLanguage);
        //  常量的字母都是大于，上面是的引用的文件名
        $appLanguage = ucfirst($appLanguage);
        if ($is_loading && defined($appLanguage) && array_key_exists($code, constant($appLanguage))) {
            $langArr = constant($appLanguage)[$code];
            if (is_array($langArr) && array_key_exists($languageType, $langArr)) {
                return $langArr[$languageType];
            }
        }
    }
    return '';
}

/**
 * 判断是否已经加载
 * @param $file_path - 加载文件的路径
 * @return bool
 */
function isLoadingFile($file_path)
{
    static $loadExistsList = [];
    strpos($file_path, '.') > 0 ?: $file_path .= FILE_EXT;
    $file_md5 = md5($file_path);
    if (array_key_exists($file_md5, $loadExistsList)) {
        return true;
    } elseif (pathinfo($file_path) && file_exists($file_path)) {
        $loadExistsList[$file_md5] = $file_path;
        /** @noinspection PhpIncludeInspection */
        require_once("$file_path");
        return true;
    }
    return false;
}
