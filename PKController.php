<?php
/**
 * 通用控制器
 * User: Administrator
 * Date: 2018/12/26
 * Time: 10:15
 */

namespace PKCore;

use function PKCore\Route\language;

abstract class PKController
{
    protected static $pageIndex, $pageSize;

    abstract public function Main();

    /**
     * 处理请求参数中带有翻页参数
     * @param string $indexField
     * @param string $rowsField
     */
    final protected function postByPages($indexField = 'PageIndex', $rowsField = 'PageSize')
    {
        self::$pageIndex = $this->checkPostParamIsEmpty($indexField) ? Formats::isNumeric(Request::post($indexField)) : 1;
        self::$pageSize = $this->checkPostParamIsEmpty($rowsField) ? Formats::isNumeric(Request::post($rowsField)) : 10;
    }

    /**
     * 处理请求参数有时间区间参数，转为 SQL 查询参数
     * @param $dbViewStartField
     * @param null $dbViewEndField
     * @param string $startTimeField
     * @param string $endTimeField
     * @return array
     */
    final protected function postByDateIntervalToSQLParam($dbViewStartField, $dbViewEndField = null,
                                                          $startTimeField = 'StartTime', $endTimeField = 'EndTime')
    {
        $result = [];
        $startTimeFieldIsEmpty = Request::has($startTimeField, 'post');
        $endTimeFieldIsEmpty = Request::has($endTimeField, 'post');
        if ($startTimeFieldIsEmpty && $endTimeFieldIsEmpty) {
            $startTime = strtotime(Request::post($startTimeField));
            $endTime = strtotime(Request::post($endTimeField));
            $startTime < $endTime ?: Statics::error(500481);
            // 如果数据表查询的字段为相同字段可为 null
            !empty($dbViewEndField) ?: $dbViewEndField = $dbViewStartField;
            $result[] = "unix_timestamp(`{$dbViewStartField}`) >= '{$startTime}'";
            $result[] = "unix_timestamp(`{$dbViewEndField}`) <= '{$endTime}'";
        }
        return $result;
    }

    final protected function checkPostParamIsEmpty($field, $tips = null)
    {
        $isNotEmpty = Request::has($field, 'post');
        $isNotEmpty ?: ($tips == null ?: fail(language($tips)));
        if ($tips != null) {
            return is_string(Request::post($field)) ? trim(Request::post($field)) : Request::post($field);
        }
        return $isNotEmpty;
    }

    final protected function checkPostParamIsExists($field, $tips = null)
    {
        $isExists = Request::has($field, 'post', false);
        if (!$isExists) {
            // 不存在 POST 请求的参数时
            $tips == null ?: Statics::error($tips);
        }
        return $isExists;
    }
}
