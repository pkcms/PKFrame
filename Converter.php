<?php
/**
 * 转换器
 * User: wz_zh
 * Date: 2017/7/12
 * Time: 16:10
 */

namespace PKCore;

class Converter
{

    /**
     * UTF-8 编码转换成中文字符
     * @param string $utf8_str UTF-8 编码
     * @return mixed
     */
    public static function utf8ToChinese($utf8_str = '')
    {
        return preg_replace('/[\x{10000}-\x{10FFFF}]/u', '', $utf8_str);
    }

    /**
     * utf8字符转换成Unicode字符
     * @param string $utf8_str Utf-8字符
     * @return string Unicode字符
     */
    public static function utf8ToUnicode($utf8_str = '')
    {
        $unicode = (\ord($utf8_str[0]) & 0x1F) << 12;
        $unicode |= (\ord($utf8_str[1]) & 0x3F) << 6;
        $unicode |= (\ord($utf8_str[2]) & 0x3F);
        return \dechex($unicode);
    }

    /**
     * 以下方法可以将Unicode编码的中文转换成utf8编码的中文，且对原来就是utf8编码的中文没影响：
     * @param $str
     * @return null|string|string[]
     */
    public static function decodeUnicode($str)
    {
        return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', \create_function('$matches', 'return iconv("UCS-2BE","UTF-8",pack("H*", $matches[1]));'), $str);
    }

    /**
     * @param $str -原始中文字符串
     * @param string $encoding 原始字符串的编码，默认GBK
     * @param string $prefix 编码后的前缀，默认"&#"
     * @param string $postfix 编码后的后缀，默认";"
     * @return string
     */
    public static function unicodeEncode($str, $encoding = 'GBK', $prefix = '&#', $postfix = ';')
    {
        $str = iconv($encoding, 'UCS-2', $str);
        $arrstr = str_split($str, 2);
        $unistr = '';
        for ($i = 0, $len = count($arrstr); $i < $len; $i++) {
            $dec = hexdec(bin2hex($arrstr[$i]));
            $unistr .= $prefix . $dec . $postfix;
        }
        return $unistr;
    }

    /**
     * @param string $unistr Unicode编码后的字符串
     * @param string $encoding 原始字符串的编码，默认GBK
     * @param string $prefix 编码字符串的前缀，默认"&#"
     * @param string $postfix 编码字符串的后缀，默认";"
     * @return string
     */
    public static function unicodeDecode($unistr, $encoding = 'GBK', $prefix = '&#', $postfix = ';')
    {
        $arruni = explode($prefix, $unistr);
        $unistr = '';
        for ($i = 1, $len = count($arruni); $i < $len; $i++) {
            if (strlen($postfix) > 0) {
                $arruni[$i] = substr($arruni[$i], 0, strlen($arruni[$i]) - strlen($postfix));
            }
            $temp = intval($arruni[$i]);
            $unistr .= ($temp < 256) ? chr(0) . chr($temp) : chr($temp / 256) . chr($temp % 256);
        }
        return iconv('UCS-2', $encoding, $unistr);
    }

    /**
     * Unicode字符转换成utf8字符
     * @param String $unicode_str
     * @return string
     */
    public static function unicodeToUtf8($unicode_str = '')
    {
        $code = \intval(\hexdec($unicode_str));
        //这里注意转换出来的code一定得是整形，这样才会正确的按位操作
        $ord_1 = \decbin(0xe0 | ($code >> 12));
        $ord_2 = \decbin(0x80 | (($code >> 6) & 0x3f));
        $ord_3 = \decbin(0x80 | ($code & 0x3f));
        $utf8_str = \chr(\bindec($ord_1)) . \chr(\bindec($ord_2)) . \chr(\bindec($ord_3));
        return $utf8_str;
    }

    /**
     * 隐藏部分字符串
     * @param $str
     * @param string $replacement
     * @param int $start
     * @param int $length
     * @return string
     */
    public static function SubstrHideReplace($str, $replacement = '*', $start = 1, $length = 3)
    {
        $len = mb_strlen($str,'utf-8');
        if ($len > intval($start+$length)) {
            $str1 = mb_substr($str,0,$start,'utf-8');
            $str2 = mb_substr($str,intval($start+$length),NULL,'utf-8');
        } else {
            $str1 = mb_substr($str,0,1,'utf-8');
            $str2 = mb_substr($str,$len-1,1,'utf-8');
            $length = $len - 2;
        }
        $new_str = $str1;
        for ($i = 0; $i < $length; $i++) {
            $new_str .= $replacement;
        }
        $new_str .= $str2;
        return $new_str;
    }

    /**
     * 自动判断把gbk或gb2312编码的字符串转为utf8
     * 能自动判断输入字符串的编码类，如果本身是utf-8就不用转换，否则就转换为utf-8的字符串
     * 支持的字符编码类型是：utf-8,gbk,gb2312
     * @param string $string - 被转换字符串
     * @return string - 输出后的 utf-8 字符串
     */
    public static function gbkToUtf8($string = '')
    {
        $charset = \mb_detect_encoding($string, array('UTF-8', 'GBK', 'GB2312'));
        $charset = \strtolower($charset);
        if ('cp936' == $charset) {
            $charset = 'GBK';
        }
        if ('utf-8' != $charset) {
            $string = \iconv($charset, 'UTF-8//IGNORE', $string);
        }
        return $string;
    }

    /**
     * JSON 格式压缩，针对 版本5.6
     * @param array $data
     * @param bool $isNumber
     * @return false|string
     */
    public static function jsonEnCode($data = array(), $isNumber = true)
    {
        if (version_compare(phpversion(), '5.6.0', '>=')) {
            return json_encode($data, $isNumber ? JSON_NUMERIC_CHECK | JSON_UNESCAPED_UNICODE : JSON_UNESCAPED_UNICODE);
        }
        return json_encode($data);
    }

    /**
     * 返回固定的日期时间格式
     * @param mixed $time 被处理的时间字符
     * @param string $format 想要的格式
     * @return false|string
     */
    public static function date($time = SYS_TIME, $format = 'Y-m-d H:i:s')
    {
        return date($format, is_numeric($time) ? $time : strtotime($time));
    }

    /**
     * 第一个词首字母小写
     * @param string $str 被处理的字符串
     * @return string
     */
    public static function lcfirst($str = '')
    {
        return strtolower(preg_replace("/([^a-z])*(.*)/u", "$1", $str)) . preg_replace("/([^a-z])*([a-z])(.*)/u", "$2$3", $str);
    }

    /**
     * 将数据转换为字符串
     * 示例 ： a=1&b=1…
     * @param array $array
     * @return string
     */
    public static function arrayToJoinString(array $array = array())
    {
        if (is_array($array)) {
            $tmp_array = array();
            foreach ($array as $key => $value) {
                $tmp_array[] = $key . '=' . urlencode($value);
            }
            return implode('&', $tmp_array);
        }
        return '';
    }

    /**
     * 将字符串转换为数据
     * @param $arrayString
     * @return array
     */
    public static function stringToArray($arrayString)
    {
        if (empty($arrayString) == false) {
            $result = array();
            $arr = explode('&', $arrayString);
            foreach ($arr as $item) {
                $item_arr = explode('=', $item);
                $result[$item_arr[0]] = $item_arr[1];
            }
            return $result;
        }
        return array();
    }

    /**
     * 将 Object 转换成 Array
     * @param null $object_str 对象字符
     * @return array|null
     */
    public static function objectToArray($object_str = null)
    {
        if (is_object($object_str)) {
            $object_str = get_object_vars($object_str);
        }
        if (is_array($object_str)) {
            foreach ($object_str as $key => $value) {
                $object_str[$key] = self::objectToArray($value);
            }
        }
        return $object_str;
    }

    /**
     * 返回输入数组中某个单一列的值。
     * @param array $array 指定要使用的多维数组（记录集）
     * @param string $column 需要返回值的列
     * @param null $index_key 作为返回数组的索引/键的列
     * @return array 返回一个数组
     */
    public static function arrayColumn($array = array(), $column = '', $index_key = null)
    {
        $result = array();
        if (is_array($array) && count($array) > 0) {
            if (version_compare(phpversion(), '5.5.0', '>=')) {
                $result = array_column($array, $column, $index_key);
            } elseif (!empty($column) && !is_null($index_key)) {
                $index_key_arr = self::arrayGetByKey($array, $index_key);
                $column_arr = self::arrayGetByKey($array, $column);
                if (is_array($column_arr) && is_array($index_key_arr) && count($column_arr) == count($index_key_arr)) {
                    $result = array_combine($index_key_arr, $column_arr);
                }
            } elseif (!empty($column) && is_null($index_key)) {
                settype($array, 'array');
                $level = arrayLevel($array);
                if ($level == 1) {
                    foreach ($array AS $one) {
                        if (is_array($one))
                            $result[@$one[$column]] = $one;
                        else
                            $result[@$one->$column] = $one;
                    }
                } elseif ($level == 2) {
                    foreach ($array AS $one) {
                        if (is_array($one)) {
                            if (false == isset($result[@$one[$column]]))
                                $result[@$one[$column]] = array();
                            $result[@$one[$column]][] = $one;
                        } else {
                            if (false == isset($result[@$one->$column]))
                                $result[@$one->$column] = array();
                            $result[@$one->$column][] = $one;
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * 根据某一特定键(下标)取出一维或多维数组的所有值；不用循环的理由是考虑大数组的效率，
     * 把数组序列化，然后根据序列化结构的特点提取需要的字符串
     * @param array $array 被处理的数组
     * @param string $string 下标
     * @return array
     */
    private static function arrayGetByKey(array $array = array(), $string = '')
    {
        if (!trim($string)) {
            return array();
        }
        preg_match_all("/\"$string\";\w{1}:(?:\d+:|)(.*?);/", serialize($array), $res);
        // 去除多余的引号
        if (isset($res[1]) && is_array($res[1])) {
            foreach ($res[1] as $key => $value) {
                $res[1][$key] = str_replace('"', null, $value);
            }
        }
        return $res[1];
    }

    /**
     * 数组转换成可以保存的数组字符
     * @param array $var 数组
     * @param boolean $isSoft 是否排序
     * @return string
     */
    public static function arrayToSaveString($var = array(), $isSoft = TRUE)
    {
        if (is_array($var)) {
            $string_start = "<?php\nreturn\t";
            if ($isSoft) {
                ksort($var);
            }
            $string_process = var_export($var, TRUE);
            $string_end = "\n?>";
            return $string_start . $string_process . $string_end;
        }
        return null;
    }

    /**
     * Xml 转 数组, 包括根键，忽略空元素和属性，尚有重大错误
     * @param null $xml
     * @return array
     */
    public static function xmlToArray($xml = null)
    {
        $reg = "/<(\\w+)[^>]*?>([\\x00-\\xFF]*?)<\\/\\1>/";
        if (preg_match_all($reg, $xml, $matches)) {
            $count = count($matches[0]);
            $arr = array();
            for ($i = 0; $i < $count; $i++) {
                $key = $matches[1][$i];
                $val = self::xmlToArray($matches[2][$i]);  // 递归
                if (array_key_exists($key, $arr)) {
                    if (is_array($arr[$key])) {
                        if (!array_key_exists(0, $arr[$key])) {
                            $arr[$key] = array($arr[$key]);
                        }
                    } else {
                        $arr[$key] = array($arr[$key]);
                    }
                    $arr[$key][] = $val;
                } else {
                    $arr[$key] = $val;
                }
            }
            return $arr;
        } else {
            return $xml;
        }
    }

    /**
     * 简单的递归
     * @param $array
     * @param string $fieldName
     * @param string $parentFieldName
     * @param int $parentId
     * @return array
     */
    public static function recursion($array, $fieldName = '', $parentFieldName = '', $parentId = 0)
    {
        $arr = array();
        foreach ($array as $v) {
            if ($v[$parentFieldName] == $parentId) {
                $temp = self::recursion($array, $fieldName, $parentFieldName, $v[$fieldName]);
                if ($temp) {
                    $v['child'] = $temp;
                }
                $arr[] = $v;
            }
        }
        return $arr;
    }

    /**
     * 无限分级, 判断不同级的数据结构
     * @access  public
     * @param   array &$data 数据库里取得的结果集 地址引用
     * @param   integer $pid 父级id的值
     * @param   string $col_id 自增id字段名（对应&$data里的字段名）
     * @param   string $col_pid 父级字段名（对应&$data里的字段名）
     * @param   string $col_cid 是否存在子级字段名（对应&$data里的字段名）
     * @return array|null  返回整理好的数组
     */
    public static function recursionPlus(array &$data, $pid = 0, $col_id = 'id', $col_pid = 'parent', $col_cid = 'hasChild')
    {
        $childs = self::findChild($data, $pid, $col_pid);
        if (empty($childs)) {
            return null;
        }
        foreach ($childs as $key => $val) {
            if (isset($val[$col_cid]) && !empty($val[$col_cid])) {
                $treeList = self::recursionPlus($data, $val[$col_id], $col_id, $col_pid, $col_cid);
                if ($treeList !== null) {
                    $childs[$key]['child'] = $treeList;
                }
            }
        }
        return $childs;
    }

    /**
     * 多维数组的递归
     * @param array $array 数组
     * @param int $pid 父级id的值
     * @param string $col_pid 自增id字段名
     * @return array
     */
    private static function findChild(&$array = array(), $pid = 0, $col_pid = 'parent')
    {
        $rootList = array();
        foreach ($array as $key => $val) {
            if ($val[$col_pid] == $pid) {
                $rootList[] = $val;
                unset($array[$key]);
            }
        }
        return $rootList;
    }

    /**
     * 字节数转换成带单位的文件大小字符串
     * @param int $size
     * @param null $unitFormat
     * @return string
     */
    public static function bytesToUnit($size = 0, $unitFormat = NULL)
    {
        // 单位列表
        $Units_arr = ['b', 'kb', 'mb', 'gb', 'tb'];
        $key = array_search($unitFormat, $Units_arr);
        $size /= pow(1024, $key);
        return number_format($size, 3) . $unitFormat;
    }

}