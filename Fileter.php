<?php
/**
 * 过滤器
 * User: wz_zh
 * Date: 2017/7/12
 * Time: 14:34
 */

namespace PKCore;


class Fileter
{

    /**
     * URL 安全过滤函数
     * @param $string
     * @return string
     */
    public static function urlReplace($string)
    {
        $string = str_replace('%20', '', $string);
        $string = str_replace('%27', '', $string);
        $string = str_replace('%2527', '', $string);
        $string = str_replace('*', '', $string);
        $string = str_replace('"', '&quot;', $string);
        $string = str_replace("'", '', $string);
        $string = str_replace('"', '', $string);
        $string = str_replace(';', '', $string);
        $string = str_replace('<', '&lt;', $string);
        $string = str_replace('>', '&gt;', $string);
        $string = str_replace("{", '', $string);
        $string = str_replace('}', '', $string);
        $string = str_replace('\\', '', $string);
        return $string;
    }

    /**
     * 强化 addslashes 操作，支持对数组解析
     * @param $string
     * @param int $force
     * @return string
     */
    public static function addslashes($string, $force = 1)
    {
        if (is_array($string) && count($string) > 0) {
            $keys = array_keys($string);
            foreach ($keys as $key) {
                $val = $string[$key];
                unset($string[$key]);
                $string[addslashes($key)] = self::addslashes($val, $force);
            }
        } elseif (!empty($string) && is_string($string)) {
            $string = addslashes($string);
        }
        return $string;
    }

    /**
     * 强化 stripslashes 操作，支持对数组解析
     * @param String $string 需要处理的字符串或数组
     * @return mixed
     */
    public static function stripslashes($string = '')
    {
        if (!is_array($string)) {
            return stripslashes($string);
        }
        foreach ($string as $key => $val) {
            $string[$key] = self::stripslashes($val);
        }
        return $string;
    }

    /**
     * HTML转义字符
     * @param string $data 字符串
     * @param null $flags
     * @return array|mixed|string 返回转义好的字符串
     */
    public static function htmlspecialchars($data = '', $flags = null)
    {
        if (is_array($data) && count($data)) {
            foreach ($data as $key => $val) {
                $data[$key] = self::htmlspecialchars($val, $flags);
            }
        } else {
            if ($flags === NULL) {
                $data = str_replace(array('&', '"', '<', '>'), array('&amp;', '&quot;', '&lt;', '&gt;'), $data);
                if (strpos($data, '&amp;#') !== false) {
                    $data = preg_replace('/&amp;((#(\d{3,5}|x[a-fA-F0-9]{4}));)/', '&\\1', $data);
                }
            } else {
                $data = \htmlspecialchars($data, $flags, 'UTF-8');
            }
        }
        return $data;
    }

    /**
     * HTML反转义字符
     * @param string $data
     * @return string
     */
    public static function htmlspecialchars_decode($data = '')
    {
        if (is_array($data) && count($data)) {
            foreach ($data as $key => $val) {
                $data[$key] = self::htmlspecialchars_decode($val);
            }
        } else {
            $data = \htmlspecialchars_decode(\htmlspecialchars_decode($data, ENT_QUOTES), ENT_QUOTES);
        }
        return $data;
    }

    /**
     * 防火墙 - 检查并过滤变量中非法字符
     * @param string|array $str 要检查的变量
     * @param bool $is_html 是否保留 HTML 标签
     * @return array|mixed|string
     */
    public static function filterValue($str = null, $is_html = FALSE)
    {
        if (gettype($str) == "string" && $is_json = Formats::isJson($str)) {
            $json_arr = Converter::objectToArray($is_json);
            if (Formats::isArray($json_arr)) {
                foreach ($json_arr as $k => $v) {
                    $is_json_v = Formats::isJson($v);
                    $json_arr[$k] = $is_json_v == false ? self::filterValue($v, $is_html) : $is_json_v;
                }
                return $json_arr;
            } else {
                return $json_arr;
            }
        } elseif (gettype($str) == "object") {
            return $str;
        } elseif (Formats::isArray($str)) {
            foreach ($str as $k => $v) {
                $str[$k] = self::filterValue($v, $is_html);
            }
            return $str;
        } else {
            if ($is_html) {
                $str = strip_tags($str);
            }
            $str = htmlspecialchars($str, ENT_QUOTES);
            if (get_magic_quotes_gpc() == FALSE) {
                $str = self::addslashes($str);
            } elseif (!empty($str)) {
                $str = stripcslashes($str);
            }
            $str = str_replace('){', '){ ', $str);
            $str = str_replace('\\', '', $str);
            return @trim($str);
        }
    }

    /**
     * 字符串加密、解密函数
     * @param string $string 字符串
     * @param bool $operation true为加密，false为解密，可选参数，默认为ENCODE
     * @param string $key 密钥：数字、字母、下划线
     * @param int $expiry 过期时间
     * @return string
     */
    public static function base64Encode($string = '', $operation = false, $key = '', $expiry = 0)
    {
        $key_length = 4;
        $fixedkey = hash('md5', $key);
        $egiskeys = md5(substr($fixedkey, 16, 16));
        $runtokey = $key_length ? ($operation ? substr(hash('md5', microtime(true)), -$key_length) : substr($string, 0, $key_length)) : '';
        $keys = hash('md5', substr($runtokey, 0, 16) . substr($fixedkey, 0, 16) . substr($runtokey, 16) . substr($fixedkey, 16));
        $string = $operation ? sprintf('%010d', $expiry ? $expiry + TIMESTAMP : 0) . substr(md5($string . $egiskeys), 0, 16) . $string : base64_decode(substr($string, $key_length));
        $result = '';
        $string_length = strlen($string);
        for ($i = 0; $i < $string_length; $i++) {
            $result .= chr(ord($string{$i}) ^ ord($keys{$i % 32}));
        }
        if ($operation) {
            return $runtokey . str_replace('=', '', base64_encode($result));
        } else {
            if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - TIMESTAMP > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $egiskeys), 0, 16)) {
                return substr($result, 26);
            } else {
                return '';
            }
        }
    }

    /**
     * 使用 HMAC 中的 sha256 算法生成带有密钥的哈希值
     * @param string $data 要进行哈希运算的消息
     * @param string $key 使用 HMAC 生成信息摘要时所使用的密钥
     * @return string 生成带有密钥的哈希值
     */
    public static function HmacSha256($data = '', $key = '')
    {
        return !is_string($data) || !is_string($key) ?: hash_hmac('sha256', $data, $key, false);
    }

}