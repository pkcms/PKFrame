<?php
/**
 * PK框架 -- 程序初始化
 * User: zhanghui
 * Date: 2016/5/16
 * Time: 15:37
 */

namespace PKCore\Init {

    /**
     * 初始化时创建框架必备的目录
     */
    function siteDir()
    {
        $dir_name = substr(PATH_PK, strlen(PATH_ROOT));
        $siteIndexFileName = 'index.php';
        if (!file_exists(PATH_ROOT . $siteIndexFileName)) {
            $codeStr = '<?php require_once ".' . DS . $dir_name . 'Index.php";';
            file_put_contents(PATH_ROOT . $siteIndexFileName, $codeStr);
            header("location:" . DOMAIN);
            exit();
        }
        $site_all_dir = array('PKConfig', 'PKController', 'PKStorage', 'PKExtend',
            'PKModel', 'PKPhar');
        foreach ($site_all_dir as $dir_name) {
            $dir_path = PATH_ROOT . $dir_name;
            file_exists($dir_path) ?: \PKCore\Files::mkdir($dir_path);
        }
    }

    /**
     * 产生可配置的文件
     */
    function configInc()
    {
        $base = PATH_PK . 'Init' . DS . 'Config.php';
        if (file_exists($base)) {
            /** @noinspection PhpIncludeInspection */
            include_once "{$base}";
            if (isset($Config)) {
                \PKCore\Files::putContents(PATH_ROOT . 'PKConfig', 'Config.php', $Config);
            }
        }
        $db = PATH_PK . 'Init' . DS . 'DBConfig.php';
        if (file_exists($db)) {
            /** @noinspection PhpIncludeInspection */
            include_once "{$db}";
            if (isset($DBConfig)) {
                \PKCore\Files::putContents(PATH_ROOT . 'PKConfig', 'DBConfig.php', $DBConfig);
            }
        }
        $lang = PATH_PK . 'Init' . DS . 'Language.php';
        if (file_exists($lang)) {
            /** @noinspection PhpIncludeInspection */
            include_once "{$lang}";
            if (isset($langCode)) {
                \PKCore\Files::putContents(PATH_ROOT . 'PKConfig', 'Language.php', $langCode);
            }
        }
        $autoLoadCode = <<<PHP
<?php
// config custom auto loading files
PHP;
        \PKCore\Files::putContents(PATH_ROOT . 'PKConfig', 'AutoLoad.php', $autoLoadCode);
    }
}